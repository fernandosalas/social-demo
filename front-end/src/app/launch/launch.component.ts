import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-launch',
  templateUrl: './launch.component.html',
  styleUrls: ['./launch.component.css']
})
export class LaunchComponent implements OnInit {

  constructor(
  	public dataService: DataService,
	  private router: Router
  	) {
  		if(dataService.isLoggedIn()){
      		this.router.navigate(['/inicio/']);
      }
  }

  ngOnInit() {
  }

}
