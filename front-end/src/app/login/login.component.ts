import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from '../service/data.service';
import { Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DataService, FormBuilder],
})

export class LoginComponent implements OnInit {

  branchesObservable: Observable<object>;
  statuslogin: any;
  focusin = true;
  rForm: FormGroup;
  post: any;
  usernameAlert = 'Please fill username';
  passwordAlert = 'Please fill password';
  loginAlert = 'El usuario y/o contraseña son incorrectos, Intente de nuevo';
  loginError = false;
  returnUrl: string;
  savedUser: string;
  savedPass: string;

  constructor(
   public router: Router,
   public dataService: DataService,
   private ChangeDetectorRef: ChangeDetectorRef,
   public zone: NgZone) {
    if(dataService.isLoggedIn()){
      this.router.navigate(['/inicio/']);
    }
  }

  ngOnInit() {
    this.savedUser = localStorage.getItem('form_user');
    this.savedPass = localStorage.getItem('form_pass');
    const that = this;
    $(document).ready(function() {
      $('#loginform_username').focus();
      $('#frm_login').validate({
        errorClass: 'form-control-feedback',
        validClass: 'form-control-feedback',
        errorElement: 'span',
        highlight: function(element) {
          $(element).parents('div.form-group').addClass('has-danger').removeClass('has-success');
          $('.error').addClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-danger');
        },
        unhighlight: function(element) {
          $(element).parents('div.form-group').removeClass('has-danger').addClass('has-success');
          $('.error').removeClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-success');
        },
        rules: {
          // simple rule, converted to {required:true}
          'Login[username]': 'required',
          // compound rule
          'Login[password]': {
            required: true,
            // password: true
          }
        },
        submitHandler: function(form, event) {
          event.preventDefault();
          that.login($(form).serializeFormJSON());
        }
      });
    });

  }


  login(form) {
    const post = {
      data: {
        username: form.username,
        password: form.password,
        remember: form.rememberMe
      },
      action: '/site/login'
    };
    this.dataService.server(post).subscribe(
    res => {
      if (res.status) {
        this.loginError = false;
        if (form.rememberMe === 1) {
          localStorage.setItem('form_user', form.username);
          localStorage.setItem('form_pass', form.password);
        } else {
          localStorage.setItem('form_user', '');
          localStorage.setItem('form_pass', '');
        }
        localStorage.setItem('token', res.token);
        localStorage.setItem('usr_id', res.usr_id);
        localStorage.setItem('usr_name', res.usr_data.full_name);
        localStorage.setItem('usr_email', res.usr_data.username);
        localStorage.setItem('usr_profile', res.usr_data.profile_photo);
        localStorage.setItem('usr_cover', res.usr_data.cover_photo);
        localStorage.setItem('usr_country', res.usr_data.country);
        localStorage.setItem('usr_city', res.usr_data.city);
        localStorage.setItem('sitio_pagina', 'inicio');
        this.zone.run(() => {
          this.router.navigate(['/inicio/']).then(() => {});
        });
      } else {
        this.loginError = true;
        this.ChangeDetectorRef.detectChanges();
      }
    },
    err => {
      return err;
    });
  }

}
