import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from '../service/data.service';
import { interval, Subscription } from 'rxjs';

declare var $: any;

@Component({
	selector: 'app-barrasuperior',
	templateUrl: './barrasuperior.component.html',
	styleUrls: ['./barrasuperior.component.css'],
	providers: [DataService, FormBuilder]
})

export class BarrasuperiorComponent implements OnInit {
	title = 'Gallo Real';
	misGruposItems: any
	adminGruposItems: any
	nombre: String;
	imagenPerfil: string;
  	usuarioId: any;
	notificationsItems = [];
	subscription: Subscription;


	constructor(public router: Router, private route: ActivatedRoute, public dataService: DataService) {
		this.getProfileData();
	}

	ngOnInit(): void {
		
		const source = interval(10000);
		const text = 'Your Text Here';
		this.subscription = source.subscribe(
			val => this.loadNotifications()
		);

	}


	prepareRoute(outlet) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
	}
	toggleSideBar() {
		var body_1 = $('body');
		body_1.toggleClass("menuclose");
	}
	cambiarMenu(nombre: string) {
		localStorage.setItem('sitio_pagina', nombre);
	}
	getClass(menu: string) {
		if (this.getPagina() === menu) {
			return 'active';
		} else {
			return '';
		}
	}
	getPagina() {
		return localStorage.getItem('sitio_pagina');
	}
	logOut() {
		localStorage.removeItem('token');
		localStorage.removeItem('usr_id');
		localStorage.removeItem('sitio_pagina');
		localStorage.removeItem('usr_name');
		this.router.navigate(['/login']);
	}
	isAuth() {
		if (localStorage.getItem('token') == undefined || localStorage.getItem('usr_id') == undefined) {
			return false;
		}
		return true;
	}
	getUsrId() {
		return localStorage.getItem('usr_id');
	}
	loadMisGruposItems() {
		var post = {
			action: '/group/grupos',
			data: {}
		};
		this.dataService.server(post).subscribe(
			res => {
				this.misGruposItems = res;
			},
			err => {
				return err;
			}
		);
	}
	loadAdminGruposItems() {
		var post = {
			action: '/group/admin',
			data: {}
		};
		this.dataService.server(post).subscribe(
			res => {
				this.adminGruposItems = res;
			},
			err => {
				return err;
			}
		);
	}
	loadNotifications(){
		var post = {
			action: '/notification',
			data: {
				idUsuario: this.getUsrId()
			}
		};
		this.dataService.server(post).subscribe(
			res => {
				res.forEach(notification => {

				});
				this.notificationsItems = res;
			},
			err => {
				return err;
			}
		);
	}
	goToNotification(notification){
		var post = {
			action: '/notification/read',
			data: {
				idNotificacion: notification.id_notification
			}
		};
		this.dataService.server(post).subscribe(
			res => {
				let arr = notification.Link.split('#');
				let route = arr[0];
				let anchor = arr[1];
				this.router.navigate([route],{ fragment: anchor });
			},
			err => {
				return err;
			}
		);
	}

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          this.nombre  = res.data.full_name;
          const profilePhoto = res.data.profile_photo;
          const token = res.data.auth_key;
          this.usuarioId = res.data.usr_id;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + token;
          }
        }
      },
      err => {
        return err;
      });

  }

}
