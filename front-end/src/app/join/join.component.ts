import { NgModule, Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../service/data.service';
import { Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css'],
  providers: [DataService, FormBuilder ]})
export class JoinComponent implements OnInit {
  //@ViewChild('username') el: ElementRef;
   branchesObservable : Observable<object> ; 

  statuslogin: any;
  focusin: boolean = true;
  rForm: FormGroup;
  post: any;
  loginAlert: string = "El usuario y/o contraseña son incorrectos, Intente de nuevo" ;
  loginError: boolean = false;
  returnUrl: string;
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public authenticationservice: DataService,
   private  ChangeDetectorRef: ChangeDetectorRef
  ) {
    /*this.rForm = fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
    });*/
  }
  ngOnInit() {

    var that = this;
    
    $(document).ready(function() {

      $("#frm_invite").validate({
        errorClass: 'form-control-feedback',
        validClass: 'form-control-feedback',
        errorElement: 'span',
        highlight: function(element, errorClass, validClass) {
          $(element).parents("div.form-group")
            .addClass('has-danger')
            .removeClass('has-success');
          $(".error").addClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-danger');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents("div.form-group")
            .removeClass('has-danger')
            .addClass('has-success');
          $(".error").removeClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-success');
        },
        rules: {
          // simple rule, converted to {required:true}
          // compound rule
          "Invite[email]": {
            required: true,
            email:true
            //password: true
          }
        }, 
  submitHandler: function(form, event) {
          event.preventDefault();
          that.invite($(form).serialize());
 }
    
    //this.authenticationservice.logout();
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/index';
  });
 });
}

invite(form)  {
  var component = this;

   this.authenticationservice.invite(form).subscribe(
          res => {
            console.log(res);
            if(res.status == true)
              {
                 this.loginError = false;
                 this.router.navigate(['/register', {token: res.token, email: res.email }]);
              }else{

                this.loginError = true;
                this.loginAlert = res.errors.email[0];
                this.ChangeDetectorRef.detectChanges();
               console.log(this.loginError);
                //that.loginAlert = res.message;
              }
          },
           err => {
            return err;
              
          }
        
      );
 }

}
