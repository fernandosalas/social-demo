import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../service/data.service';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { ResponseCommentComponent } from '../response-comment/response-comment.component';
import { EditPostComponent } from '../edit-post/edit-post.component';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  postItems: any;
  usuarioId;
  imagenPerfil: string;
  loginError: boolean = false;

  constructor (
   public router: Router,
   public dataService: DataService,
   protected sanitizer: DomSanitizer,
   public dialog: MatDialog,public ChangeDetectorRef: ChangeDetectorRef
   ) {

  }

  ngOnInit(){
    this.getProfileData();
    $('.sidebar-left').removeClass('hide');
    $('.navbar-fixed').removeClass('hide');

  }

  loadPostItems() {
    const post = {
      action: '/post/ultimas',
      data: {
        idUsuario: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        res.forEach( publicacion => {
          publicacion.post_content = this.sanitizer.bypassSecurityTrustHtml(publicacion.post_content);
          publicacion.created_at = formatDate(new Date(publicacion.created_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
          publicacion.comments.forEach(comment => {
            comment.created_at = formatDate(new Date(comment.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            comment.responses.forEach(response => {
              response.create_at = formatDate(new Date(response.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            });
          });
        });
        this.postItems = res;
      },
      err => {
        return err;
      }
    );
  }
  checkVerMas(verMas: boolean, content: string | any[]) {
    if (verMas != true && content.length > 200) {
      return true;
    } else {
      return false;
    }
  }

  responderComentario(post, comment){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      idTarget: comment.id_comment,
      postTarget: post
    }
    this.dialog.open(ResponseCommentComponent, dialogConfig);
  }

  cargarFormularioEditar(post) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      postId: post.post_id,
      postContent: post.post_content
    }
    this.dialog.open(EditPostComponent, dialogConfig);
  }

  comentar(element: string, type: string, postTarget: any, key: number) {
    let content = (<HTMLInputElement>document.getElementById(element)).value;
    var post = {
      data: {
        'Comment[content]': content,
        'Comment[id_target]': postTarget.post_id,
        'Comment[type]': type
      },
      action: '/comment/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (res.success == true) {
          (<HTMLInputElement>document.getElementById(element)).value = "";
          this.postItems[key]['comments'].push(res.data[0]);
          // Create Notification for Comment
          let link = '';
          if(postTarget.created_by_group > 0){
            link = '/groupDash/' + postTarget.created_by_group + '#post-' + postTarget.post_id;
          }else{
            link = '/dashboard/' + postTarget.created_by + '#post-' + postTarget.post_id;
          }
          this.createNotification(localStorage.getItem('usr_name') + ' comentó en tu publicación.','comment',link,postTarget.created_by);
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  createNotification(text,type,link,user_target){
    var post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target:user_target,
        created_by: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(res => {},err => {});
  }

  eliminarElementos(keyPost, targetType, idTarget, keyComment = 0) {
    if (targetType == 'post') {
      var post = {
        action: '/post/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems.splice(keyPost, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
    if (targetType == 'comment') {
      var post = {
        action: '/comment/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems[keyPost]['comments'].splice(keyComment, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
  }

  calificarElemento(id_target, key, target = 'comment', indexcomment = 0, value) {
    var post = {
      data: {
        'Like[id_target]': id_target,
        'Like[type]': target,
        'Like[value]': value
      }, action: '/like/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (target == 'post') {
          this.postItems[key]['like'] = res.value;
          this.postItems[key]['prom'] = res.prom;
        }
        else if (target == 'comment') {
          this.postItems[key]['comments'][indexcomment]['like'] = res.value;
          this.postItems[key]['comments'][indexcomment]['prom'] = res.prom;
        }
      },
      err => {
        return err;
      }
    );
  }

  guardarEdicionComentario(id_comment, commentIndex, postIndex) {
    var content = (<HTMLInputElement>document.getElementById('comment_txt_' + id_comment)).value;
    var post = {
      action: '/comment/update',
      data: {
        'Comment[content]': content
      },
      urlParams: '&id=' + id_comment
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[postIndex]['comments'][commentIndex]['content'] = res.data.content;
        this.postItems[postIndex]['comments'][commentIndex].ModoEditar = false;
      },
      err => {
        return err;
      });
  }

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          const profilePhoto = res.data.profile_photo;
          const token = res.data.auth_key;
          this.usuarioId = res.data.usr_id;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + token;
          }
          this.loadPostItems();
        }
      },
      err => {
        return err;
      });

  }

}
