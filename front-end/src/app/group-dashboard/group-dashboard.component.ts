import { Component, OnInit, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../service/data.service';
import { UploadOutput, UploadInput, UploadFile, UploaderOptions } from 'ngx-uploader';
import { DomSanitizer } from '@angular/platform-browser';
import { formatDate } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EditPostComponent } from '../edit-post/edit-post.component';
import { ResponseCommentComponent } from '../response-comment/response-comment.component';

declare var $: any;
declare var autosize: any;

@Component({
  selector: 'app-group-dashboard',
  templateUrl: './group-dashboard.component.html',
  styleUrls: ['./group-dashboard.component.css'],
  providers: [DataService]
})
export class GroupDashboardComponent implements OnInit {

  idGroup: number;
  nameGroup: string;
  profileGroup: string;
  coverGroup: string;
  numberMembers = 0;
  numberRequests = 0;
  unirseGrupo = 99;
  miembrosItems: any;
  solicitudesItems: any;
  adminGrupo = false;
  postItems: any;
  usuarioId: any;
  imgsTempNuevoPost: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  dragOver: boolean;
  token: string;
  imagenPerfil: string;
  loginError = false;
  options: UploaderOptions;

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, public  router: Router, public dataService: DataService, public ChangeDetectorRef: ChangeDetectorRef, protected sanitizer: DomSanitizer, public dialog: MatDialog) {
    this.options = { concurrency: 1, maxUploads: 10 };
    this.imgsTempNuevoPost = [];
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
  }

  ngOnInit() {
    $('.sidebar-left').removeClass('hide');
    $('.navbar-fixed').removeClass('hide');
    this.getProfileData();
    this.route.fragment.subscribe(f => {
      setTimeout( () => {
        const element = document.querySelector('#' + f);
        if (element) { element.scrollIntoView({
          behavior: 'smooth',
        });
        }
      }, 1000);
    });
    this.route.params.subscribe( params => {
      this.idGroup = params['idGroup'];
      this.getInfoGrupo();
      this.loadAdminGruposItems();
      this.loadSolicitudesItems();
      this.loadMiembrosItems();
      this.loadPostItems();
    });
  }

  getInfoGrupo() {
    const post = {
      action: '/group/get',
      data: {
        idGroup: this.idGroup
      }
    };
    this.dataService.server(post).subscribe(res => {
      const group = res[0];
      this.nameGroup = group.nameGroup;
      const profilePhoto = group.profile_photo;
      const coverPhoto = group.cover_photo;
      if (profilePhoto == null || profilePhoto === '') {
        this.profileGroup = 'assets/img/guest.png';
      } else {
        // tslint:disable-next-line:max-line-length
        this.profileGroup  = this.dataService.url + '/group/cargarimagenperfil?archivo=' + profilePhoto + '&group_id=' + this.idGroup + '&auth_key=' + this.token;
      }
      if (coverPhoto == null || coverPhoto === '') {
        this.coverGroup = 'assets/img/banner4.png';
      } else {
        // tslint:disable-next-line:max-line-length
        this.coverGroup = this.dataService.url + '/group/cargarimagenportada?archivo=' + coverPhoto + '&group_id=' + this.idGroup + '&auth_key=' + this.token;
      }
    });
  }

  loadAdminGruposItems() {
    const post = {
      action: '/group/admin',
      data: {}
    };
    this.dataService.server(post).subscribe(res => {
      const search = res.find(x => x.idGroup === this.idGroup);
      if (search !== undefined) {
        this.adminGrupo = true;
        this.unirseGrupo = 1;
      } else {
        this.loadMisGruposItems();
      }
    },
    err => {
      return err;
    });
  }

  loadMisGruposItems() {
    const post = {
      action: '/group/grupos',
      data: {}
    };
    this.dataService.server(post).subscribe(res => {
      const search = res.find(x => x.idGroup === this.idGroup);
      if (search !== undefined) {
        this.adminGrupo = false;
        this.unirseGrupo = 2;
      }
    },
    err => {
      return err;
    });
  }

  loadSolicitudesItems() {
    const post = {
      action: '/group/solicitudes',
      data: {
        idGroup: this.idGroup
      }
    };
    this.dataService.server(post).subscribe( res => {
        res.forEach( element => {
          const profilePhoto = element.profile_photo;
          if (profilePhoto == null || profilePhoto === '') {
            element.profile_photo = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            element.profile_photo = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + element.idUser + '&auth_key=' + this.token;
          }
        });
      this.solicitudesItems = res;
      this.numberRequests = res.length;
      const search = res.find(x => x.idUser === localStorage.getItem('usr_id'));
      if (search !== undefined) {
        this.unirseGrupo = 3;
      }
    },
    err => {
      return err;
    });
  }

  loadMiembrosItems() {
    const post = {
      action: '/group/miembros',
      data: {
        idGroup: this.idGroup
      }
    };
    this.dataService.server(post).subscribe( res => {
      res.forEach( element => {
        const profilePhoto = element.profile_photo;
        if (profilePhoto == null || profilePhoto === '') {
          element.profile_photo = 'assets/img/user-header.png';
        } else {
          // tslint:disable-next-line:max-line-length
          element.profile_photo = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + element.idUser + '&auth_key=' + this.token;
        }
      });
      this.miembrosItems = res;
      this.numberMembers = res.length;
      const search = res.find(x => x.idUser === localStorage.getItem('usr_id'));
      if (search === undefined) {
        this.unirseGrupo = 0;
      }
    },
    err => {
      return err;
    });
  }

  onUploadOutput(output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        const event: UploadInput = {
          type: 'uploadAll',
          // tslint:disable-next-line:max-line-length
          url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id=' + localStorage.getItem('usr_id'),
          method: 'POST',
          fieldName: 'UploadForm[images]'
        };
        this.uploadInput.emit(event);
      break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined' ) {
          this.imgsTempNuevoPost.push(output.file);
        }
      break;
      case 'uploading':
      break;
      case 'removed':
        this.imgsTempNuevoPost = this.imgsTempNuevoPost.filter((file: UploadFile) => file !== output.file);
      break;
      case 'dragOver':
        this.dragOver = true;
      break;
      case 'dragOut':
      break;
      case 'drop':
        this.dragOver = false;
      break;
      case 'done':
      // The file is downloaded
      break;
      case 'rejected':
      break;
    }
  }

  src_imgMiniatura(imagen) {
    // tslint:disable-next-line:max-line-length
    return imagen.response !==  undefined  ?  this.dataService.url + '/post/cargarimagen?file=' + imagen.response.archivo + '&auth_key=' + this.token  : '' ;
  }

  publicar(textarea: string) {
    const imagenesPost = [];
    if (this.imgsTempNuevoPost.length > 0) {
      this.imgsTempNuevoPost.forEach(function (element) {
        imagenesPost.push(element.response.archivo);
      });
    }
    const post = {
      data: {
        'Post[post_content]': textarea,
        'imagenesPost': imagenesPost,
        'created_by_group': this.idGroup,
        visibility: 1,
        wall_target: this.idGroup
      },
      action: '/post/create'
    };
    this.dataService.server(post).subscribe(
    res => {
      if (res.success === true) {
        this.loadPostItems();
        (<HTMLInputElement>document.getElementById('publicacion_txt')).value = '';
        this.imgsTempNuevoPost = [];
        // Create notification for post on group wall
        const postTarget = res.post[0];
        const link = '/groupDash/' + this.idGroup + '#post-' + postTarget.post_id;
        this.miembrosItems.forEach( member => {
          if (member.aceptada === 1) {
            // tslint:disable-next-line:max-line-length
            this.createNotification(localStorage.getItem('usr_name') + ' ha publicado en el grupo ' + this.nameGroup, 'post', link, member.idUser);
          }
        });
      } else {
        this.loginError = true;
        this.ChangeDetectorRef.detectChanges();
      }
    },
    err => {
      return err;
    });
  }

  loadPostItems() {
    const post = {
      action: '/post',
      data: {
        idGroup: this.idGroup
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        res.forEach( publication => {
          publication.post_content = this.sanitizer.bypassSecurityTrustHtml(publication.post_content);
          publication.created_at = formatDate(new Date(publication.created_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
          publication.comments.forEach(comment => {
            comment.created_at = formatDate(new Date(comment.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            comment.responses.forEach(response => {
              response.create_at = formatDate(new Date(response.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            });
          });
        });
        this.postItems = res;
      },
      err => {
        return err;
      });
  }

  linkify(text) {
    const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(urlRegex, function (url) {
      if (url.includes('https://www.youtube.com/watch?v=')) {
        url = url.split('https://www.youtube.com/watch?v=');
        // tslint:disable-next-line:max-line-length
        return '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + url[1] + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
      } else {
        return '';
      }
    });
  }

  cargarFormularioEditar(post) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      postId: post.post_id,
      postContent: post.post_content
    };
    this.dialog.open(EditPostComponent, dialogConfig);
  }

  eliminarElementos(keyPost, targetType, idTarget, keyComment = 0) {
    if (targetType === 'post') {
      const post = {
        action: '/post/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems.splice(keyPost, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
    if (targetType === 'comment') {
      const post = {
        action: '/comment/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems[keyPost]['comments'].splice(keyComment, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
  }

  calificarElemento(id_target, key, target = 'comment', indexcomment = 0, value) {
    const post = {
      data: {
        'Like[id_target]': id_target,
        'Like[type]': target,
        'Like[value]': value
      }, action: '/like/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (target === 'post') {
          this.postItems[key]['like'] = res.value;
          this.postItems[key]['prom'] = res.prom;
        } else if (target === 'comment') {
          this.postItems[key]['comments'][indexcomment]['like'] = res.value;
          this.postItems[key]['comments'][indexcomment]['prom'] = res.prom;
        }
      },
      err => {
        return err;
      }
    );
  }

  guardarEdicionComentario(id_comment, commentIndex, postIndex) {
    const content = (<HTMLInputElement>document.getElementById('comment_txt_' + id_comment)).value;
    const post = {
      action: '/comment/update',
      data: {
        'Comment[content]': content
      },
      urlParams: '&id=' + id_comment
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[postIndex]['comments'][commentIndex]['content'] = res.data.content;
        this.postItems[postIndex]['comments'][commentIndex].ModoEditar = false;
      },
      err => {
        return err;
      });
  }

  checkVerMas(verMas, content) {
    return verMas !== true && content.length > 200;
  }

  responderComentario(post, comment) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      idTarget: comment.id_comment,
      postTarget: post
    };
    this.dialog.open(ResponseCommentComponent, dialogConfig);
  }

  setFocus(id) {
    $(document).ready(function () {
      const element = <HTMLInputElement>document.getElementById(id);
      (element).focus();
      autosize(element);
    });
  }

  comentar(element: string, type: string, postTarget: any, key: number) {
    const content = (<HTMLInputElement>document.getElementById(element)).value;
    const post = {
      data: {
        'Comment[content]': content,
        'Comment[id_target]': postTarget.post_id,
        'Comment[type]': type
      },
      action: '/comment/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (res.success === true) {
          (<HTMLInputElement>document.getElementById(element)).value = '';
          this.postItems[key]['comments'].push(res.data[0]);
          // Create Notification for Comment
          let link = '';
          if (postTarget.created_by_group > 0) {
            link = '/groupDash/' + postTarget.created_by_group + '#post-' + postTarget.post_id;
          } else {
            link = '/dashboard/' + postTarget.created_by + '#post-' + postTarget.post_id;
          }
          this.createNotification(localStorage.getItem('usr_name') + ' comentó en tu publicación.', 'comment', link, postTarget.created_by);
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  createNotification(text, type, link, user_target) {
    const post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target: user_target,
        created_by: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(() => {}, () => {});
  }

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          const profilePhoto = res.data.profile_photo;
          this.token = res.data.auth_key;
          this.usuarioId = res.data.usr_id;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
          }
        }
      },
      err => {
        return err;
      });

  }

  enviarSolicitud() {

  }
}
