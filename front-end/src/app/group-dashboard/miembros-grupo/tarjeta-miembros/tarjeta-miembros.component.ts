import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-tarjeta-miembros]',
  templateUrl: './tarjeta-miembros.component.html',
  styleUrls: ['./tarjeta-miembros.component.css']
})
export class TarjetaMiembrosComponent {

  @Input() idUsuario: number;
  @Input() idSolicitud: number;
  @Input() nombre: number;
  @Input() fotoPerfil: string;
  @Input() agregar: boolean;
  @Input() esSolicitud: boolean;
  @Input() solicitudEnviada = false;
  @Input() solicitudAceptada = false;

  constructor(public  dataService: DataService, public  router: Router) { }

  verMuro() {
    this.router.navigate(['/dashboard/' + this.idUsuario]).then(() => {});
  }

  aceptarSolicitud() {
    const post = {
      action: '/mygroup/aceptar',
      data: {
        idSolicitud: this.idSolicitud,
        idUsuario: localStorage.getItem('usr_id')
      }
    };
    this.dataService.server(post).subscribe(() => {
      location.reload();
    },
    err => {
        return err;
    });
  }

  rechazarSolicitud() {
    const post = {
      action: '/mygroup/rechazar',
      data: {
        idSolicitud: this.idSolicitud,
        idUsuario: localStorage.getItem('usr_id')
      }
    };
    this.dataService.server(post).subscribe(
    () => {
      location.reload();
    },
    err => {
        return err;
    });
  }
}
