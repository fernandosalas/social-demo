import { Component, OnInit, ChangeDetectorRef, Injector, EventEmitter, NgZone } from '@angular/core';
import {Router} from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from '../service/data.service';
import { Observable } from 'rxjs';
import { NgSelectConfig } from '@ng-select/ng-select';
import { UploadInput, UploadOutput, UploadFile } from 'ngx-uploader';

declare var $: any;

interface AngSelectEvent {
  name: string;
  value: any;
}

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css'],
  providers: [DataService, FormBuilder ]
})
export class ConfigComponent implements OnInit {
  // @ViewChild('username') el: ElementRef;
  branchesObservable: Observable<object> ;
  events: AngSelectEvent[] = [];
  statuslogin: any;
  focusin = true;
  rForm: FormGroup;
  post: any;
  countries: any;
  cities: any;
  selectedCities: any;
  idCity: number;
  userCountry = '';
  loginAlert = 'El usuario y/o contraseña son incorrectos, Intente de nuevo' ;
  loginError = false;
  returnUrl: string;
  token: string;
  public componentData: any = '';
  public userSettings: any = { 'geoTypes': ['(cities)']  , 'inputPlaceholderText': 'Ciudad y país', 'showRecentSearch': false };

  uploadInput: EventEmitter<UploadInput>;
  dragOver: boolean;
  nameProfile: string;
  nameCover: string;
  nombre: string;
  correo: string;
  country: string;
  city: string;

constructor(
  public router: Router,
  public authenticationservice: DataService,
  public dataService: DataService,
  private  ChangeDetectorRef: ChangeDetectorRef,
  private config: NgSelectConfig,
  private injector: Injector
  ) {
  this.config.notFoundText = 'No se encontraron ciudades';
  this.uploadInput = new EventEmitter<UploadInput>();
}
ngOnInit() {
  this.getProfileData();
  $('.sidebar-left').removeClass('hide');
  $('.navbar-fixed').removeClass('hide');
  this.getCountries();
  this.userSettings = Object.assign({}, this.userSettings);
  this.ChangeDetectorRef.detectChanges();
  let that = this;
  $(document).ready(function() {
    $('#frm_config').validate({
      errorClass: 'form-control-feedback',
      validClass: 'form-control-feedback',
      errorElement: 'span',
      highlight: function(element) {
        $(element).parents('div.form-group').addClass('has-danger').removeClass('has-success');
        $('.error').addClass('form-control-feedback');
        $(element).closest('input').addClass('form-control-danger');
      },
      unhighlight: function(element) {
        $(element).parents('div.form-group').removeClass('has-danger').addClass('has-success');
        $('.error').removeClass('form-control-feedback');
        $(element).closest('input').addClass('form-control-success');
      },
      rules: {
        'User[password]': { 
          required: false,
          pwcheck: false,
          minlength: 8
        },
      },
      messages: {
        'User[password]': {
        pwcheck: 'La contraseña debe de contener minimo 8 caracteres ademas de contener numeros y letras'
        },
      },
      submitHandler: function(form) {
        that.register($(form).serializeFormJSON());
      }
    });
    $.validator.addMethod('pwcheck', function(value) {
     return /^[A-Za-z0-9\d=!\-@._#*]*$/.test(value) // consists of only these
     && /[a-z]/.test(value) // has a lowercase letter
     && /\d/.test(value); // has a digit
    });
  });
}

getProfileData() {
  const post = {
    action: '/user/my-profile-data'
  };

  this.dataService.server(post).subscribe(res => {
      if (res.success) {
         this.nombre  = res.data.full_name;
         this.correo  = res.data.username;
         this.country = res.data.country;
         this.city    = res.data.city;
      }
    },
    err => {
      return err;
  });

}

getCountries() {
  const post = {
    action: '/country',
    data: {}
  };
  this.dataService.server(post).subscribe(
    res => {
      this.countries = res;
    },
    err => {
      return err;
  });
}

selectCountry(country_iso) {
  return this.country === country_iso;
}

register(form)  {
  const post = {
    data: form,
    action: '/config/update'
  };
  this.authenticationservice.server(post).subscribe(
    res => {
      if (res.status === true) {
        this.loginError = false;
        
        const routerService = this.injector.get(Router);
        const ngZone = this.injector.get(NgZone);

        localStorage.setItem('token', res.token);
        localStorage.setItem('usr_id', res.usr_id);
        this.router.navigate(['dashboard/' + res.usr_id]);
        //this.router.navigate(['dashboard']);
     } else {
      this.loginError = true;
      this.loginAlert = res.msg;
      this.ChangeDetectorRef.detectChanges();
    }
  },
  err => {
    return err;
  });
}

onUploadProfileImage(output: UploadOutput) {
  switch (output.type) {
    case 'allAddedToQueue':
      const event: UploadInput = {
        type: 'uploadAll',
        url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id=' + localStorage.getItem('usr_id'),
        method: 'POST',
        fieldName: 'UploadForm[images]'
      };
      this.uploadInput.emit(event);
    break;
    case 'addedToQueue':
      if (typeof output.file !== 'undefined' ) {
        this.nameProfile = output.file.name;
      }
    break;
    case 'uploading':
    break;
    case 'removed':
      this.nameProfile = '';
    break;
    case 'dragOver':
      this.dragOver = true;
    break;
    case 'dragOut':
    break;
    case 'drop':
      this.dragOver = false;
    break;
    case 'done':
    break;
    case 'rejected':
    break;
  }
}

onUploadCoverImage(output: UploadOutput) {
  switch (output.type) {
    case 'allAddedToQueue':
      const event: UploadInput = {
        type: 'uploadAll',
        url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id=' + localStorage.getItem('usr_id'),
        method: 'POST',
        fieldName: 'UploadForm[images]'
      };
      this.uploadInput.emit(event);
    break;
    case 'addedToQueue':
      if (typeof output.file !== 'undefined' ) {
        this.nameCover = output.file.name;
      }
    break;
    case 'uploading':
    break;
    case 'removed':
      this.nameCover = '';
    break;
    case 'dragOver':
      this.dragOver = true;
    break;
    case 'dragOut':
    break;
    case 'drop':
      this.dragOver = false;
    break;
    case 'done':
    break;
    case 'rejected':
    break;
  }
}



}
