import { map } from 'rxjs/operators';
import { Location } from '@angular/common';
import { NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import { Router } from '@angular/router';



@NgModule({
  providers: [HttpClientModule]
})

export class DataService { 
  public url: string
  constructor(
    private http: HttpClient, 
    private loc: Location,
    public router: Router
    ) {
    this.url =  this.loc.path() === 'localhost' ?  'http://social.api.juancker.com/web' : 'http://social.api.juancker.com/web';
   }

   isLoggedIn(){
       let token = localStorage.getItem('token');
       return  token ? true : false ;
   }

  invite(post): Observable<any> {
    let json = JSON.stringify(post);
    let action = "web/site/register";
    json = json.replace(/['"]+/g, '');
    let params = json;
    var headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')

    return this.http.post(this.url + action, params, { headers }
    ).pipe(
      map(
        data => {
          return data;
        },
        error => {
          console.log("Error", error);
        }
      ));
  }

   register(post, token): Observable<any> {
      let json = JSON.stringify(post);
      let action = "web/site/register";

      //El backend recogerá un parametro json
      json = json.replace(/['"]+/g, ''); //This will remove all the quotes
      let params = json;
      console.log(params);

      var headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')

      return this.http.post(this.url + action, params, { headers }
      ).pipe(
        map(
          data => {
            return data;
          },
          error => {
            console.log("Error", error);
          }
        ));
    }

    server(post): Observable<any> {
      if(post.data == null){
        post.data = {}
    }

    var urlParams = (typeof post.urlParams === 'undefined') ? '' : post.urlParams;
    var urlvars = Object.keys(post.data).map(function(k) {
      return encodeURIComponent(k) + '=' + encodeURIComponent(post.data[k])
    }).join('&');

    var tokenParam = localStorage.getItem('token') !== null ? '?auth_key=' + localStorage.getItem('token'): '';
     + urlParams;
    //Establecemos cabeceras
    var headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8' ),  observe: 'response';
    return this.http.post(this.url + post.action + tokenParam, urlvars, { headers } 
    ).pipe( map(
      (data: any) => {
        if(data.code !== null && data.code == 401 ){ 
           localStorage.removeItem('token');
           localStorage.removeItem('usr_id');
           this.router.navigate(['/login']).then(() => {});
        }
        return data;
      },
      error => {
       // console.log("Error", status);
      },
    )
    );
  }

  logout() {
    localStorage.removeItem('usr_id');
    localStorage.removeItem('usr_data');
  }



  getItem(): void {
    const getItemUrl = this.url + '';

  }

}
