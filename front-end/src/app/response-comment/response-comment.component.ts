import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-response-comment',
  templateUrl: './response-comment.component.html',
  providers: [DataService]
})

export class ResponseCommentComponent implements OnInit {

  idTarget: string;
  postTarget: any;
  content: string;
  usuarioId = localStorage.getItem('usr_id');

  // tslint:disable-next-line:max-line-length
  constructor(public dialogRef: MatDialogRef<ResponseCommentComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) { }

  ngOnInit() {
    this.idTarget = this.data.idTarget;
    this.postTarget = this.data.postTarget;
  }

  guardarComentario() {
    if (this.idTarget !== null && this.content != null && this.content !== '') {
      const post = {
        action: '/comment/respond' ,
        data: {
          id_target: this.idTarget,
          content: this.content,
          type: 'response',
          created_by: localStorage.getItem('usr_id'),
          isActive: 1
        }
      };
      this.dataService.server(post).subscribe(() => {
          // Create Notification for Comment
          let link;
          if (this.postTarget.created_by_group > 0) {
            link = '/groupDash/' + this.postTarget.created_by_group + '#post-' + this.postTarget.post_id;
          } else {
            link = '/dashboard/' + this.postTarget.created_by + '#post-' + this.postTarget.post_id;
          }
          // tslint:disable-next-line:max-line-length
          this.createNotification(localStorage.getItem('usr_name') + ' comentó en tu publicación.', 'comment', link, this.postTarget.created_by);
        },
        err => {
          return err;
        }
      );
    }
  }

  createNotification(text, type, link, user_target) {
    const post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target: user_target,
        created_by: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(() => {
      location.reload();
    }, () => {});
  }

  cerrarModal() {
    this.dialogRef.close();
  }
}
