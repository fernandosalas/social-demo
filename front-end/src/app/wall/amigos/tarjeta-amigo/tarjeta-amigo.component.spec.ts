import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetaAmigoComponent } from './tarjeta-amigo.component';

describe('TarjetaAmigoComponent', () => {
  let component: TarjetaAmigoComponent;
  let fixture: ComponentFixture<TarjetaAmigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetaAmigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetaAmigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
