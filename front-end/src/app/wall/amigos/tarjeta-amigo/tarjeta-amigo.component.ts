import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { Router,  ActivatedRoute } from '@angular/router';

@Component({
  selector: '[app-tarjeta-amigo]',
  templateUrl: './tarjeta-amigo.component.html',
  styleUrls: ['./tarjeta-amigo.component.css']
})

export class TarjetaAmigoComponent implements OnInit {

  imagenPerfil: string;

  @Input() idUsuario: number;
  @Input() idSolicitud: number;
  @Input() nombre: number;
  @Input() agregar: boolean;
  @Input() esSolicitud: boolean;
  @Input() solicitudEnviada: boolean = false;
  @Input() rechazada: boolean = false;
  @Input() solicitudAceptada: boolean = false;


  constructor (public  dataService: DataService, public  router: Router) {}

    ngOnInit() {
      this.cargarDatosUsuario(this.idUsuario);
    }

  cargarDatosUsuario(idUsuario) {
    const post = {
      action: '/user/obtener',
      data: {
        idUsuario: Number(idUsuario)
      }
    };
    this.dataService.server(post).subscribe(res => {
        this.nombre = res[0].full_name;
        const profilePhoto = res[0].profile_photo;
        if (profilePhoto === null) {
          this.imagenPerfil = 'assets/img/user-header.png';
        } else {
          // tslint:disable-next-line:max-line-length
          this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + idUsuario + '&auth_key=' + localStorage.getItem('token');
        }
      },
      err => {
        return err;
      }
    );
  }

  verMuro(){
    this.router.navigate(["/dashboard/" + this.idUsuario  ]);
  }

  enviarSolicitud(){
    var post = {
      action: '/friend/agregar',
      data: {
        idUsuario: localStorage.getItem('usr_id'),
        idFriend: this.idUsuario
      }
    };
   this.dataService.server(post).subscribe(
      res => {
        this.solicitudEnviada = res.success;
      },
      err => {
        return err;
      }
    );

     event.stopPropagation();
  }

  createNotification(text,type,link,user_target){
    var post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target:user_target,
        created_by: localStorage.getItem('usr_id')
      }
    };
    this.dataService.server(post).subscribe(res => {
      //location.reload();
    },err => {});
  }

  aceptarSolicitud(event){
   
    var post = {
      action: '/friend/aceptar',
      data: {
        idSolicitud: this.idSolicitud,
        idUsuario: localStorage.getItem('usr_id')
      },
    };
    this.dataService.server(post).subscribe(
    (res) => {
      this.solicitudAceptada = res.success;
    },
    err => {
        return err;
    }); 
    event.stopPropagation();
  }

  rechazarSolicitud(event){


    var post = {
      action: '/friend/rechazar',
      data: {
        idSolicitud: this.idSolicitud,
        idUsuario: localStorage.getItem('usr_id')
      },
    };
    this.dataService.server(post).subscribe(
    (res) => {
        if(res.success){ 
          this.rechazada = true;
          this.solicitudAceptada = false; 
        }
    },
    err => {
        return err;
    });
    event.stopPropagation();
  }
  

}
