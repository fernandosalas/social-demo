import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-amigos',
  templateUrl: './amigos.component.html',
  styleUrls: ['./amigos.component.css']
})

export class AmigosComponent implements OnInit {

  friendItems: any ;
  numberFriends: number;
  idUsuario: number;
  buscarAmigos: boolean;

  constructor(private route: ActivatedRoute, public  dataService: DataService) {}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.idUsuario = + params['idUsuario'];
			this.buscarAmigos = Number(localStorage.getItem('usr_id')) === this.idUsuario;
			this.cargarMisAmigos();
    });
	}

	cargarMisAmigos() {
		 const post = {
		  action: '/friend',
		  data: {
				idUsuario: this.idUsuario
			}
		};
		this.dataService.server(post).subscribe(res => {
				 this.friendItems = res;
				 this.numberFriends = res.length;
			 },
			 err => {
			  return err;
			}
		);
	}

}
