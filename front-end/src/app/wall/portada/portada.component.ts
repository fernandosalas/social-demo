import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-portada',
  templateUrl: './portada.component.html',
  styleUrls: ['./portada.component.css']
})
export class PortadaComponent implements OnInit {

  nombre: string;
  usuarioId: string;
  token: any;
  imagenPerfil: string;
  imagenPortada: string;
  country: string;
  agregarAmigo = 99;

  constructor(private route: ActivatedRoute, public  dataService: DataService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.usuarioId =  params['idUsuario'];
      this.token = localStorage.getItem('token');
      const userSession = localStorage.getItem('usr_id');
      if (this.usuarioId === userSession) {
        this.getProfileData();
      } else {
        this.cargarDatosUsuario();
      }
    });
  }

  isOwner(){

   return this.usuarioId == localStorage.getItem('usr_id');

  }

  cargarDatosUsuario() {
    const post = {
      action: '/user/obtener',
      data: {
        idUsuario: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(res => {
        this.nombre = res[0].full_name;
        const profilePhoto = res[0].profile_photo;
        const coverPhoto = res[0].cover_photo;
        if (profilePhoto == null || profilePhoto === '') {
          this.imagenPerfil = 'assets/img/user-header.png';
        } else {
          // tslint:disable-next-line:max-line-length
          this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
        }
        if (coverPhoto == null || coverPhoto === '') {
          this.imagenPortada = 'assets/img/banner3.png';
        } else {
          // tslint:disable-next-line:max-line-length
          this.imagenPortada = this.dataService.url + '/post/cargarimagenportada?archivo=' + coverPhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
        }
      },
      err => {
      console.log(err);
        return err;
      }
    );
  }

  cargarMisAmigos() {
    const post = {
     action: '/friend',
     data: {
       idUsuario: this.usuarioId
     }
   };
   this.dataService.server(post).subscribe(
       res => {
         if(res){ 
            const search = res.find(x => x.idFriend === localStorage.getItem('usr_id'));
            if (search !== undefined) {
              this.agregarAmigo = 1;
            }
            if (this.agregarAmigo === 99) {
              if (localStorage.getItem('usr_id') !== this.usuarioId) {
                this.agregarAmigo = 0;
              }
            }
        }
      },
      err => {
       return err;
     }
   );
 }

 cargarSolicitudes() {
  const post = {
    action: '/friend/solicitudes',
    data: {
      idUsuario: this.usuarioId
    }
  };
  this.dataService.server(post).subscribe(
    res => {
      const search = res.find(x => x.idUsuario === localStorage.getItem('usr_id'));
      if (search !== undefined) {
        this.agregarAmigo = 2;
      }
    },
    err => {
      return err;
    }
  );
}

  enviarSolicitud() {
    const post = {
      action: '/friend/agregar',
      data: {
        idUsuario: localStorage.getItem('usr_id'),
        idFriend: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(
      () => {
        // Create notification for friend request
        // tslint:disable-next-line:max-line-length
        this.createNotification(this.nombre + ' te ha enviado una solicitud de amistad.', 'solicitud de amistad', 'buscaramigos', this.usuarioId);
      },
      err => {
        return err;
      }
    );
  }

  createNotification(text, type, link, user_target) {
    const post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target: user_target,
        created_by: localStorage.getItem('usr_id')
      }
    };
    this.dataService.server(post).subscribe(() => {
      location.reload();
    }, () => {});
  }

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          this.nombre = res.data.full_name;
          console.log(this.token);
          this.usuarioId = res.data.usr_id;
          this.country = res.data.country;
          const profilePhoto = res.data.profile_photo;
          const coverPhoto = res.data.cover_photo;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
          }
          if (coverPhoto == null || coverPhoto === '') {
            this.imagenPortada = 'assets/img/banner3.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPortada = this.dataService.url + '/post/cargarimagenportada?archivo=' + coverPhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
          }
          this.cargarSolicitudes();
          this.cargarMisAmigos();
        }
      },
      err => {
        return err;
      });

  }

}
