import { Component, OnInit, EventEmitter, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../service/data.service';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { formatDate } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EditPostComponent } from '../edit-post/edit-post.component';
import { ResponseCommentComponent } from '../response-comment/response-comment.component';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery-9';

declare var $: any;
declare var autosize: any;

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.css']
})


export class WallComponent implements OnInit {

  @Input() wallType: string;
  @Input() wallOptions: any = [];
  @Input() idUsuario: number;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];



  post: any;
  loginError = false;
  returnUrl: string;
  postItems: any;
  comments: any;
  friendItems: any;
  addFriendItems: any;
  options: UploaderOptions;
  formData: FormData;
  imgsTempNuevoPost: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  token: string;
  notificacion: any;
  imagenPerfil: string;
  usuarioId: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public dataService: DataService,
    public ChangeDetectorRef: ChangeDetectorRef,
    protected sanitizer: DomSanitizer,
    public dialog: MatDialog
  ) {
    this.options = { concurrency: 1, maxUploads: 10 };
    this.imgsTempNuevoPost = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  src_imgMiniatura(imagen) {
    // tslint:disable-next-line:max-line-length
    return imagen.response !== undefined ? this.dataService.url + '/post/cargarimagen?file=' + imagen.response.archivo + '&auth_key=' + this.token : '';
  }

  onUploadOutput(output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        // uncomment this if you want to auto upload files when added
        const event: UploadInput = {
          type: 'uploadAll',
          // tslint:disable-next-line:max-line-length
          url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id = ' + localStorage.getItem('usr_id'),
          method: 'POST',
          fieldName: 'UploadForm[images]'
        };
        this.uploadInput.emit(event);
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {

          this.imgsTempNuevoPost.push(output.file);
        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.imgsTempNuevoPost.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
          this.imgsTempNuevoPost[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        this.imgsTempNuevoPost = this.imgsTempNuevoPost.filter((file: UploadFile) => file !== output.file);
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
      case 'rejected':
    }
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  ngOnInit() {
    this.getProfileData();
    $('.sidebar-left').removeClass('hide');
    $('.navbar-fixed').removeClass('hide');
    this.route.fragment.subscribe(f => {
      setTimeout( () => {
        const element = document.querySelector('#' + f);
        if (element) { element.scrollIntoView({
          behavior: 'smooth',
        });
        }
      }, 1000);
    });


    this.galleryOptions = [
            {
                width: '50%',
                height: '400px',
                thumbnailsColumns: 4,
                previewCloseOnClick: true,
                imageAnimation: NgxGalleryAnimation.Slide
            },
            // max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '600px',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false
            }
    ];

  }

  setFocus(id) {
    $(document).ready(function () {
      const element = <HTMLInputElement>document.getElementById(id);
      (element).focus();
      autosize(element);
    });
  }

  guardarEdicionComentario(id_comment, commentIndex, postIndex) {
    const content = (<HTMLInputElement>document.getElementById('comment_txt_' + id_comment)).value;
    const post = {
      action: '/comment/update',
      data: {
        'Comment[content]': content
      },
      urlParams: '&id=' + id_comment
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[postIndex]['comments'][commentIndex]['content'] = res.data.content;
        this.postItems[postIndex]['comments'][commentIndex].ModoEditar = false;
      },
      err => {
        return err;
      });
  }

  checkVerMas(verMas, content) {
    if (verMas != true && content.length > 200) {
      return true;
    } else {
      return false;
    }
  }

  loadPostItems() {
    const post = {
      action: '/post',
      data: {
        idUsuario: this.idUsuario
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        res.forEach( publicacion => {
          publicacion.post_content_html = this.sanitizer.bypassSecurityTrustHtml(this.linkify(publicacion.post_content));
          publicacion.created_at = formatDate(new Date(publicacion.created_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
          publicacion.comments.forEach(comment => {
            comment.created_at = formatDate(new Date(comment.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            comment.responses.forEach(response => {
              response.create_at = formatDate(new Date(response.create_at), 'd \'de\' MMMM \'de\' y \'a las\' H:mm', 'es-MX');
            });
          });
        });
        this.postItems = res;
      },
      err => {
        return err;
      });
  }

  linkify(text) {
    const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(urlRegex, function (url) {
      if (url.includes('https://www.youtube.com/watch?v=')) {
        url = url.split('https://www.youtube.com/watch?v=');
        return '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + url[1] + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
      } else {
        return '';
      }
    });
  }

  eliminarElementos(keyPost, targetType, idTarget, keyComment = 0) {
    if (targetType == 'post') {
      const post = {
        action: '/post/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems.splice(keyPost, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
    if (targetType == 'comment') {
      const post = {
        action: '/comment/eliminar',
        data: [],
        urlParams: '&id=' + idTarget
      };
      this.dataService.server(post).subscribe(
        res => {
          if (res.success) {
            this.postItems[keyPost]['comments'].splice(keyComment, 1);
          }
        },
        err => {
          return err;
        }
      );
    }
  }

  cargarFormularioEditar(post) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      postId: post.post_id,
      postContent: post.post_content
    };
    this.dialog.open(EditPostComponent, dialogConfig);
  }

  responderComentario(post, comment) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      idTarget: comment.id_comment,
      postTarget: post
    };
    this.dialog.open(ResponseCommentComponent, dialogConfig);
  }

  loadComments(id_target, key) {
    const post = {
      action: '/comment',
      data: {
        id_target: id_target
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[key]['comments'] = res;
      },
      err => {
        return err;
      }
    );
  }

  calificarElemento(id_target, key, target = 'comment', indexcomment = 0, value) {
    const post = {
      data: {
        'Like[id_target]': id_target,
        'Like[type]': target,
        'Like[value]': value
      }, action: '/like/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (target == 'post') {
          this.postItems[key]['like'] = res.value;
          this.postItems[key]['prom'] = res.prom;
        } else if (target == 'comment') {
          this.postItems[key]['comments'][indexcomment]['like'] = res.value;
          this.postItems[key]['comments'][indexcomment]['prom'] = res.prom;
        }
      },
      err => {
        return err;
      }
    );
  }

  loadLikes(id_target, key) {
    const post = {
      action: '/like',
      data: {
        id_target: id_target,
        type: 'post'
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[key]['likes'] = res.likes;
      },
      err => {
        return err;
      }
    );
  }

  publicar(textarea: string) {
    const imagenesPost = [];
    if (this.imgsTempNuevoPost.length > 0) {
      this.imgsTempNuevoPost.forEach(function (element) {
        imagenesPost.push(element.response.archivo);
      });
    }
    const post = {
      data: {
        'Post[post_content]': textarea,
        'imagenesPost': imagenesPost,
        'created_by_group': 0,
        visibility: 1,
        wall_target: this.idUsuario
      },
      action: '/post/create'
    };
    this.dataService.server(post).subscribe(res => {
        if (res.success === true) {
          this.loadPostItems();
          (<HTMLInputElement>document.getElementById('publicacion_txt')).value = '';
          this.imgsTempNuevoPost = [];
          // Create notification for post on friend wall
          const userSession = Number(localStorage.getItem('usr_id'));
          if (userSession !== this.idUsuario) {
            const postTarget = res.post[0];
            const link = '/dashboard/' + this.idUsuario + '#post-' + postTarget.post_id;
            this.createNotification(localStorage.getItem('usr_name') + ' ha publicado en tu muro.', 'post', link, this.idUsuario);
          }
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  editarPublicacion(postData) {
    const post = {
      action: '/post/update',
      data: postData,
      urlParams: '&id=' + postData['id']
    };
    this.dataService.server(post).subscribe(
      res => {
        this.postItems[postData['indicePost']]['post_content'] = res.post_content;
        $('#modalEditarPublicacion').modal('hide');
      },
      err => {
        return err;
      }
    );
  }

  subirImagen(textarea: string) {
    const post = {
      data: {
        'Post[post_content]': textarea,
        visibility: 1,
        wall_target: this.idUsuario
      },
      action: '/post/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (res.success == true) {
          this.loadPostItems();
          (<HTMLInputElement>document.getElementById('puiblicacion_txt')).value = '';
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  comentar(element: string, type: string, postTarget: any, key: number) {
    const content = (<HTMLInputElement>document.getElementById(element)).value;
    const post = {
      data: {
        'Comment[content]': content,
        'Comment[id_target]': postTarget,
        'Comment[type]': type
      },
      action: '/comment/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        if (res.success == true) {
          (<HTMLInputElement>document.getElementById(element)).value = '';
          this.postItems[key]['comments'].push(res.data[0]);
          // Create Notification for Comment
          let link = '';
          if (postTarget.created_by_group > 0) {
            link = '/groupDash/' + postTarget.created_by_group + '#post-' + postTarget.post_id;
          } else {
            link = '/dashboard/' + postTarget.created_by + '#post-' + postTarget.post_id;
          }
          this.createNotification(localStorage.getItem('usr_name') + ' comentó en tu publicación.', 'comment', link, postTarget.created_by);
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  createNotification(text, type, link, user_target) {
    const post = {
      action: '/notification/create',
      data: {
        text: text,
        type: type,
        link: link,
        user_target: user_target,
        created_by: this.usuarioId
      }
    };
    this.dataService.server(post).subscribe(res => {}, err => {});
  }

  likePublicacion(type: string, id_target: number, key: number, value: number) {
    const post = {
      data: {
        'Like[id_target]': id_target,
        'Like[type]': type,
        'Like[value]': value
      },
      action: '/like/create'
    };
    this.dataService.server(post).subscribe(
      res => {
        //Poner condiciones aqui
        if (res.success == true) {
          this.postItems[key]['likes'] = res.likes;
          this.postItems[key]['like'] = res.value;
          this.postItems[key]['prom'] = res.prom;
        } else {
          this.loginError = true;
          this.ChangeDetectorRef.detectChanges();
        }
      },
      err => {
        return err;
      }
    );
  }

  eliminarAmigo(id_target, keyFriend) {
    const post = {
      action: '/friend/eliminar',
      data: [],
      urlParams: '&id=' + id_target
    };
    this.dataService.server(post).subscribe(
      () => {
        this.friendItems.splice(keyFriend, 1);
      },
      err => {
        return err;
      }
    );
  }

  crearNotificacion(type) {
    const post = {
      action: '/notificacion/create',
      data: {
        'Notification[type]': type
      }
    };
    this.dataService.server(post).subscribe(
      res => {
        this.notificacion = res;
      },
      err => {
        return err;
      }
    );
  }

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          const profilePhoto = res.data.profile_photo;
          this.token = res.data.auth_key;
          this.usuarioId = res.data.usr_id;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + this.token;
          }
          this.loadPostItems();
        }
      },
      err => {
        return err;
      });

  }

}
