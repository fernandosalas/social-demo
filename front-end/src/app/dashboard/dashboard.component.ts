import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, OnDestroy {

  idUsuario: number;
  sub: any;

  constructor(private route: ActivatedRoute, public router: Router) {}

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(
      params => {
        this.idUsuario = + params['idUsuario'];
      }
    );
    $('.sidebar-left').removeClass('hide');
    $('.navbar-fixed').removeClass('hide');
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
