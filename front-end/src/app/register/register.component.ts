import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from '../service/data.service';
import { NgSelectConfig } from '@ng-select/ng-select';

declare var $: any;

interface AngSelectEvent {
  name: string;
  value: any;
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [DataService, FormBuilder ]

})
export class RegisterComponent implements OnInit {
  events: AngSelectEvent[] = [];
  private sub: any;
  focusin = true;
  post: any;
  countries: any;
  userCountry = '';
  loginAlert = 'El usuario y/o contraseña son incorrectos, Intente de nuevo' ;
  loginError = false;
  token: string;


constructor(
  private route: ActivatedRoute,
  public router: Router,
  public authenticationservice: DataService,
  public dataService: DataService,
  private  ChangeDetectorRef: ChangeDetectorRef,
  private config: NgSelectConfig
  ) {
  if(dataService.isLoggedIn()){
    this.router.navigate(['/inicio/']);

  }
  this.config.notFoundText = 'No se encontraron ciudades';
}



ngOnInit() {
 this.sub = this.route.params.subscribe(params => {
   this.token = params['token']; // (+) converts string 'id' to a number
 });
 this.getCountries();
 this.ChangeDetectorRef.detectChanges();
 const that = this;
 $(document).ready(function() {

  $('#frm_register').validate({
    errorClass: 'form-control-feedback',
    validClass: 'form-control-feedback',
    errorElement: 'span',
    highlight: function(element, errorClass, validClass) {
      $(element).parents('div.form-group')
      .addClass('has-danger')
      .removeClass('has-success');
      $('.error').addClass('form-control-feedback');
      $(element).closest('input').addClass('form-control-danger');
      console.log(element);
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).parents('div.form-group')
      .removeClass('has-danger')
      .addClass('has-success');
      $('.error').removeClass('form-control-feedback');
      $(element).closest('input').addClass('form-control-success');
    },
    rules: {
      'User[username]': 'required',
      'User[full_name]': 'required',
      'User[password]': {
       required: true,
       pwcheck: true,
       minlength: 8
     },
   },
   messages: {
    'User[password]': {
      pwcheck: 'La contraseña debe de contener minimo 8 caracteres ademas de contener numeros y letras'
    },
  },

  submitHandler: function(form) {
      that.register($(form).serializeFormJSON());
    }
  });

  $.validator.addMethod('pwcheck', function(value) {
   return /^[A-Za-z0-9\d=!\-@._#*]*$/.test(value) // consists of only these
   && /[a-z]/.test(value) // has a lowercase letter
   && /\d/.test(value); // has a digit
 });
});
}


getCountries() {
  const post = {
    action: '/country',
    data: {}
  };
  this.dataService.server(post).subscribe(
    res => {
      this.countries = res;
    },
    err => {
      return err;
    });
}

register(form)  {
  const post = {
    data: form,
    action: '/site/registrar'
  };
  this.authenticationservice.server(post).subscribe(res => {
      if (res.status === true) {
       this.loginError = false;
       localStorage.setItem('token', res.token);
       localStorage.setItem('usr_id', res.usr_id);
         this.router.navigate(['/dashboard/' +  res.usr_id]);
     } else {
      this.loginError = true;
      this.loginAlert = res.msg;
      this.ChangeDetectorRef.detectChanges();
    }
  }, err => {
    return err;
  });
}

/*correo()  {
  var component = this;

  var post = {
    data: {},
    action: '/site/email'
  };



  this.authenticationservice.server(post).subscribe(
    res => {
     if(res.status == true)
      {
       this.loginError = false;
       this.router.navigate(['/dashboard']);
       localStorage.setItem('token', res.token);
       localStorage.setItem('usr_id', res.usr_id);
     }else{
      this.loginError = true;
      this.loginAlert = res.msg;
      this.ChangeDetectorRef.detectChanges();
    }
  },
  err => {
    return err;

  }

  );
}*/

}
