import { Component, OnInit } from '@angular/core';
import { Subscription, Observable, interval, timer } from 'rxjs';
import { DataService } from '../service/data.service';

declare var $: any;

@Component({
  selector: 'app-buscar-amigos',
  templateUrl: './buscar-amigos.component.html',
  styleUrls: ['./buscar-amigos.component.css']
})

export class BuscarAmigosComponent implements OnInit {
	
	addFriendItems: any;	
	solicitudesAmigos: any;

	constructor(public  dataService: DataService) {}  

	ngOnInit() {
		this.cargarSugerenciasAmigos();
		this.cargarSolicitudesAmigos();
		$('.sidebar-left').removeClass('hide');
		$('.navbar-fixed').removeClass('hide');
	}

	cargarSugerenciasAmigos(){
	  var post = {
	    action: '/friend/sugerencias',
	    data: {
				idUsuario: localStorage.getItem('usr_id')
			}
	  };
	  this.dataService.server(post).subscribe(
	  res => {
	      this.addFriendItems = res;
	    },
	  err => {
	    return err;
	  });
	}

	cargarSolicitudesAmigos(){
	  var post = {
	    action: '/friend/solicitudes',
	    data: {
				idUsuario: localStorage.getItem('usr_id')
			}
	  };
	  this.dataService.server(post).subscribe(
	  res => {
	    this.solicitudesAmigos = res;
	    },
	  err => {
	    return err;
	  });
	}

}