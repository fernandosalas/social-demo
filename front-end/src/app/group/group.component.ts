import { NgModule, Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../service/data.service';
import { Observable } from 'rxjs';
import { NgSelectConfig } from '@ng-select/ng-select';
import { AppComponent } from '../app.component';

declare var $: any;

interface AngSelectEvent {
	name: string;
	value: any;
}

@Component({
	selector: 'app-group',
	templateUrl: './group.component.html',
	styleUrls: ['./group.component.css'],
	providers: [DataService, FormBuilder ]
})

export class GroupComponent implements OnInit {
	branchesObservable: Observable<object>;
	events: AngSelectEvent[] = [];
	private sub: any;
	statuslogin: any;
	focusin = true;
	rForm: FormGroup;
	post: any;
	countries: any;
	cities: any;
	selectedCities: any;
	idCity: number;
	userCountry = '';
	loginAlert = 'El usuario y/o contraseña son incorrectos, Intente de nuevo';
	loginError = false;
	returnUrl: string;
	token: string;
	userId: any;
	public componentData: any = '';
	public userSettings: any = { 'geoTypes': ['(cities)']  , 'inputPlaceholderText': 'Ciudad y país', 'showRecentSearch': false };

constructor( 
	private route: ActivatedRoute,
	public router: Router,
	public authenticationservice: DataService,
	public dataService: DataService,
	private  ChangeDetectorRef: ChangeDetectorRef,
	private config: NgSelectConfig,
  private app: AppComponent
  ) {
}

ngOnInit() {
  $('.sidebar-left').removeClass('hide');
  $('.navbar-fixed').removeClass('hide');
  const that = this;
  this.sub = this.route.params.subscribe(params => {
   this.token = params['token'];
 });
  this.userId = localStorage.getItem('usr_id');
  this.userSettings = Object.assign({}, this.userSettings);
  this.ChangeDetectorRef.detectChanges();
  $(document).ready(function() {

    $("#frm_register").validate({
      errorClass: 'form-control-feedback',
      validClass: 'form-control-feedback',
      errorElement: 'span',
      highlight: function(element, errorClass, validClass) {
        $(element).parents("div.form-group")
        .addClass('has-danger')
        .removeClass('has-success');
        $(".error").addClass('form-control-feedback');
        $(element).closest('input').addClass('form-control-danger');
        console.log(element);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parents("div.form-group")
        .removeClass('has-danger')
        .addClass('has-success');
        $(".error").removeClass('form-control-feedback');
        $(element).closest('input').addClass('form-control-success');
      },
      rules: {

        "group[naemGroup]": "required",

      },
      messages: {
        "Group[naemGroup]": {
          pwcheck: "La contraseña debe de contener minimo 8 caracteres ademas de contener numeros y letras"
        },
      },

      submitHandler: function(form, event) {
        that.register($(form).serializeFormJSON());
      }
    });

    $.validator.addMethod("pwcheck", function(value) {
     return /^[A-Za-z0-9\d=!\-@._#*]*$/.test(value) // consists of only these
     && /[a-z]/.test(value) // has a lowercase letter
     && /\d/.test(value) // has a digit
   });
  });
}

register(form)  {
  const post = {
    data: form,
    action: '/group/crear'
  };

  this.authenticationservice.server(post).subscribe(
    (res: any) => {
      if (res.status === true) {
       this.router.navigate(['/groupDash/' + res.data.idGroup ]); 
       //this.app.adminGruposItems.push(res.data);
       this.app.addGroup(res.data);
     } else {
      this.loginError = true;
      this.loginAlert = res.msg;
      this.ChangeDetectorRef.detectChanges();
    }
  },
  err => {
    return err;
  }
  );
}
}
