import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  providers: [DataService] 
})

export class EditPostComponent implements OnInit {

  postId: string;
  postContent: string;

  constructor(public dialogRef: MatDialogRef<EditPostComponent>, @Inject(MAT_DIALOG_DATA) public data: any,public dataService: DataService) { }

  ngOnInit() {
    this.postId = this.data.postId;
    this.postContent = this.data.postContent;
  }

  guardarPublicacion() {
    if (this.postId !== null && this.postContent != null && this.postContent != "") {
      var post = {  
        action: '/post/update' ,
        data: {
          post_id: this.postId,
          post_content: this.postContent,
          updated_by: localStorage.getItem('usr_id'),
          updated_at: new Date()
        }
      };
      this.dataService.server(post).subscribe(res => {
          location.reload();
        },
        err => {
          return err;
        }
      );
    }
  }

  cerrarModal() {
    this.dialogRef.close();
  }
}
