import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { slideInAnimation } from './animations';
import { DataService } from './service/data.service';

declare var $: any;

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    slideInAnimation
  ],
  providers: [DataService, FormBuilder]
})

export class AppComponent implements OnInit {
  title = 'Gallo Real'; 
  public misGruposItems: any;
  adminGruposItems: any;
  nombre: String;
  imagenPerfil: string;
  usuarioId: any;

  constructor(
    public router: Router, 
    public dataService: DataService,
    private  ChangeDetectorRef: ChangeDetectorRef
    ) {}

  prepareRoute(outlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
  ngOnInit() {
    if(this.dataService.isLoggedIn()){
       this.getProfileData();
  }
   
    $(document).ready(function () {
      var body_1 = $('body');
      if (body_1.hasClass("scroll_header") === true) {
        $(window).on("scroll", function () {
          if ($(document).scrollTop() >= 250) {
            body_1.addClass("active_scroll");
          } else {
            body_1.removeClass("active_scroll");
          }
        });
      }
      $(window).on("load", function () {

        setTimeout(function () {
          $('.progress_profile').circleProgress({
            fill: { gradient: ["#2ec7cb", "#6c8bef"] },
            lineCap: 'butt'
          });
          var myvalues = [10, 8, 5, 7, 4, 2, 8, 10, 8, 5, 6, 4, 1, 7, 4, 5, 8, 10, 8, 5, 6, 4, 4];
          $('.dynamicsparkline').sparkline(myvalues, { type: 'bar', width: '100px', height: '20', barColor: '#ffffff', barWidth: '2', barSpacing: 2 });

          var myvalues2 = [10, 8, 5, 7, 4, 2, 8, 10, 8, 5, 6, 4, 1, 7, 4, 5, 8, 10, 8, 5, 6, 4, 4, 1, 7, 4, 5, 8, 10, 8, 5, 6, 4, 4];
          $('.dynamicsparkline2').sparkline(myvalues2, { type: 'bar', width: '200px', height: '60', barColor: '#ffffff', barWidth: '3', barSpacing: 3 });
          var url = window.location;
          function menuitems() {
            if (body_1.hasClass('horizontal-menu') === false) {

              $('.sidebar-left .nav li a').on('click', function () {
                if ($(this).hasClass('menudropdown') === true) {
                  $(this).toggleClass("show").next().slideToggle().parent().addClass("in");
                }
              });
            } else {
              if ($(window).width() >= 1020) {

                $('.sidebar-left > .nav > li').on('mouseover', function () {
                  if ($(this).find('a:first-of-type').hasClass('menudropdown') == true) {
                    $(this).find('a:first-of-type').addClass("show").next().slideDown().parent().addClass("in");
                  }
                });
                $('.sidebar-left > .nav > li ').on('mouseleave', function () {
                  if ($(this).find('a:first-of-type').hasClass('menudropdown') == true) {
                    $(this).find('a:first-of-type').removeClass("show").next().slideUp().parent().removeClass("in");
                  }
                });
              } else {

                $('.sidebar-left .nav li a').on('click', function () {
                  if ($(this).hasClass('menudropdown') === true) {
                    $(this).toggleClass("show").next().slideToggle().parent().addClass("in");
                  }
                });
              }
            }
          }
          menuitems();
          $('#search_header').on('focus', function () {
            $(".search-block").show();
            body_1.addClass('searchshow'); $('#search_header').focusout();
          });
          $('.search-block .close-btn').on('click', function () {
            $(".search-block").slideUp();
            body_1.removeClass('searchshow');
          });
          $(document).keyup(function (e) {
            if (e.keyCode == 27) {
              $(".search-block").fadeOut();
              body_1.removeClass('searchshow');
              $('#search_header').focusout();
            }
          });
          $(".inboxmenu").on("click", function () {
            $(".mailboxnav ").toggleClass("mailboxnavopen");
          });


          $(".menu-collapse-right").on("click", function () {
            body_1.toggleClass("menuclose-right");
          });
          $(".menu-small").on("click", function () {
            body_1.toggleClass("menusmall");
          });

        }, 500);

      });
      $('.form-check-input').on('change', function () {
        $(this).parent().toggleClass("active")
        $(this).closest(".media").toggleClass("active");
      });
      $('.fullscreen-btn').on('click', function () {
        $(this).closest(".full-screen-container").toggleClass("fullscreen");
        body_1.toggleClass("fullscreen");
      });
      if ($(window).width() <= 1440) { body_1.addClass('menuclose menuclose-right'); }

      $(window).on('resize', function () {
        if ($(window).width() <= 1440) { body_1.addClass('menuclose menuclose-right'); }
      });
    });
  }
  cambiarMenu(nombre: string) {
    localStorage.setItem('sitio_pagina', nombre);
  }
  getClass(menu: string) {
    if (this.getPagina() === menu) {
      return 'active';
    } else {
      return '';
    }
  }
  getPagina() {
    return localStorage.getItem('sitio_pagina');
  }

  logOut() {
    localStorage.removeItem('token');  
    localStorage.removeItem('usr_id');
    localStorage.removeItem('sitio_pagina');
    this.router.navigate(['/login']);
  }

  isAuth() {
    if (localStorage.getItem('token') == undefined || localStorage.getItem('usr_id') == undefined ) {
      return false;
    }
    return true;
  }

  getUsrId() {
      return localStorage.getItem('usr_id');
  }
  loadMisGruposItems() {
    var post = {
      action: '/group/grupos',
      data: {}
    }; 
    this.dataService.server(post).subscribe(
      (res:any) => {
        this.misGruposItems = res;
      },
      err => {
        return err;
      }
    );
  }

  public addGroup(element){

      this.misGruposItems.push(element);
      this.adminGruposItems.push(element);
      this.ChangeDetectorRef.detectChanges();

  }

  loadAdminGruposItems(

   ) {
    var post = {
      action: '/group/admin',
      data: {}
    };
    this.dataService.server(post).subscribe(
      res => {
        this.adminGruposItems = res;
      },
      err => {
        return err;
      }
    );
  }

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
  };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          this.nombre  = res.data.full_name;
          const profilePhoto = res.data.profile_photo;
          const token = res.data.auth_key;
          this.usuarioId = res.data.usr_id;
          if (profilePhoto == null || profilePhoto === '') {
            this.imagenPerfil = 'assets/img/user-header.png';
          } else {
            // tslint:disable-next-line:max-line-length
            this.imagenPerfil  = this.dataService.url + '/post/cargarimagenperfil?archivo=' + profilePhoto + '&usr_id=' + this.usuarioId + '&auth_key=' + token;
          }
          this.loadMisGruposItems();
          this.loadAdminGruposItems();
        }
      },
      err => {
        return err;
      });

  }
}
