import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AuthguardService } from './service/authguard.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { BarrasuperiorComponent } from './barrasuperior/barrasuperior.component';
import { GestionarMiembrosComponent } from './group-dashboard/gestionar-miembros/gestionar-miembros.component';
import { MiembrosGrupoComponent } from './group-dashboard/miembros-grupo/miembros-grupo.component';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es-MX';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditPostComponent } from './edit-post/edit-post.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { ResponseCommentComponent } from './response-comment/response-comment.component';
import { ConfigGroupComponent } from './config-group/config-group.component';
import { NgxUploaderModule} from 'ngx-uploader';

registerLocaleData(localeES, 'es-MX');

@NgModule({
  declarations: [
    AppComponent,
    BarrasuperiorComponent,
    GestionarMiembrosComponent,
    MiembrosGrupoComponent,
    EditPostComponent,
    ResponseCommentComponent,
    ConfigGroupComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatDialogModule,
        FormsModule,
        NgxUploaderModule
    ],
  providers: [AuthguardService],
  bootstrap: [AppComponent]
})

export class AppModule { }
