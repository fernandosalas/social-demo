//import { Component, OnInit } from '@angular/core';
import { NgModule, Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from '../service/data.service';
import { Observable } from 'rxjs';

declare var $: any;
declare var autosize: any;
@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css'],
  providers: [DataService]
})
export class FriendComponent implements OnInit {
post: any;
  loginError: boolean = false;
  returnUrl: string;
 friendItems: any;   
  

  constructor(private route: ActivatedRoute,
    public router: Router,
    public dataService: DataService,
    private ChangeDetectorRef: ChangeDetectorRef,) { }

  ngOnInit() {
  	$('.sidebar-left').removeClass('hide');
    $('.navbar-fixed').removeClass('hide');
  // this.loadFriend();
    var that = this;
  }
loadFriends() {

  var data = {
    action: '/friend',
    data: {}
  };

  this.dataService.server(data).subscribe(
    res => {
      this.friendItems = res;
    },
    err => {
      return err;
    }

    );
  console.log(this.friendItems);

}


eliminarAmigo(){
var post = {
    action: '/friend/eliminarAmigo',
    data: {}
  };

  this.dataService.server(post).subscribe(
    res => {
      this.friendItems = res;
    },
    err => {
      return err;
    }

    );
}

agregarAmigo(){
var post = {
    action: '/friend/agregarAmigo',
    data: {}
  };

  this.dataService.server(post).subscribe(
    res => {
      this.friendItems = res;
    },
    err => {
      return err;
    }

    );
}



}
