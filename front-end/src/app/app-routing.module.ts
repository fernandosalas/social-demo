import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';
import { AuthguardService } from './service/authguard.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxUploaderModule } from 'ngx-uploader';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxGalleryModule } from 'ngx-gallery-9';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LaunchComponent } from './launch/launch.component';
import { JoinComponent } from './join/join.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { ConfigComponent } from './config/config.component';
import { GroupComponent } from './group/group.component';
import { MyGroupComponent } from './my-group/my-group.component';
import { FriendComponent } from './friend/friend.component';
import { GroupDashboardComponent } from './group-dashboard/group-dashboard.component';
import { WallComponent } from './wall/wall.component';
import { PortadaComponent } from './wall/portada/portada.component';
import { FotoPerfilComponent } from './wall/foto-perfil/foto-perfil.component';
import { AmigosComponent } from './wall/amigos/amigos.component';
import { TarjetaAmigoComponent } from './wall/amigos/tarjeta-amigo/tarjeta-amigo.component';
import { NotificationComponent } from './barrasuperior/notification/notification.component';
import { BuscarAmigosComponent } from './buscar-amigos/buscar-amigos.component';
import { TarjetaGrupoComponent } from './my-group/tarjeta-grupo/tarjeta-grupo.component';
import { TarjetaMiembrosComponent } from './group-dashboard/miembros-grupo/tarjeta-miembros/tarjeta-miembros.component';
import {ConfigGroupComponent} from './config-group/config-group.component';

export class CustomReuseStrategy implements RouteReuseStrategy {

    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        return false;
    }

    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): boolean {
        return false;
    }

    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        return false;
    }

    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        return false;
    }

    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        return false;
    }
}


const appRoutes: Routes = [
    { path: '', redirectTo: '/launch', pathMatch: 'full', data: { animation: 'launch' } },
    { path: 'launch', component: LaunchComponent, data: { animation: 'launch' } },
    { path: 'login', component: LoginComponent, data: { animation: 'login' } },
    { path: 'join', component: JoinComponent, data: { animation: 'join' } },
    { path: 'register', component: RegisterComponent, data: { animation: 'register' } },
     // tslint:disable-next-line:max-line-length
    { path: 'dashboard/:idUsuario', component: DashboardComponent, canActivate: [AuthguardService], data: { animation: 'slideInAnimation' } },
    { path: 'config', component: ConfigComponent, canActivate: [AuthguardService], data: { animation: 'config' } },
    { path: 'group', component: GroupComponent, canActivate: [AuthguardService], data: { animation: 'group' } },
    { path: 'mygroup', component: MyGroupComponent, canActivate: [AuthguardService], data: { animation: 'mygroup' } },
    { path: 'friend', component: FriendComponent, canActivate: [AuthguardService], data: { animation: 'friend' } },
    { path: 'groupDash/:idGroup', component: GroupDashboardComponent, canActivate: [AuthguardService], data: { animation: 'groupDash' } },
    { path: 'configGrupo/:idGroup', component: ConfigGroupComponent, canActivate:  [AuthguardService]},
    { path: 'buscaramigos', component: BuscarAmigosComponent, canActivate: [AuthguardService], data: { animation: 'buscaramigos' } },
    {
        path: 'inicio', component: HomeComponent, canActivate: [AuthguardService], data: { animation: 'inicio' },
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
        ]
    },
    // otherwise redirect to home
    { path: '**', redirectTo: '/login', data: { animation: 'login' } },
];


@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        NgSelectModule,
        NgxUploaderModule,
        NgxGalleryModule
    ],
    exports: [RouterModule],
    declarations: [
        HomeComponent,
        GroupComponent,
        FriendComponent,
        LaunchComponent,
        RegisterComponent,
        DashboardComponent,
        LoginComponent,
        JoinComponent,
        ConfigComponent,
        MyGroupComponent,
        GroupDashboardComponent,
        WallComponent,
        PortadaComponent,
        FotoPerfilComponent,
        AmigosComponent,
        TarjetaAmigoComponent,
        NotificationComponent,
        BuscarAmigosComponent,
        TarjetaGrupoComponent,
        TarjetaMiembrosComponent
    ]
    , providers: [AuthguardService, { provide: RouteReuseStrategy, useClass: CustomReuseStrategy }
    ]
})

export class AppRoutingModule {
}
