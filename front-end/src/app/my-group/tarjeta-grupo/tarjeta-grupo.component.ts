import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router,  ActivatedRoute } from '@angular/router';

@Component({
	selector: '[app-tarjeta-grupo]',
	templateUrl: './tarjeta-grupo.component.html',
	styleUrls: ['./tarjeta-grupo.component.css']
})

export class TarjetaGrupoComponent {
	@Input() idGrupo: number;
	@Input() nombre: string;
	@Input() foto: string;
	@Input() agregar: boolean;
	@Input() esSolicitud: boolean;
	@Input() solicitudEnviada = false;
	public solicitudAceptada = false;

	constructor(
		public  dataService: DataService,
		public  router: Router) { }

	verMuro() {
		this.router.navigate(['/groupDash/' + this.idGrupo  ]);
	}

	unirseGrupo() {
		const post = {
			action: '/mygroup/unir',
			data: {
				idGroup : this.idGrupo,
				idUsuario: localStorage.getItem('usr_id'),
			},
		};
		this.dataService.server(post).subscribe(
			res => {
				this.solicitudEnviada = res.success;
			},
			err => {
				return err;
			}
		);
		event.stopPropagation();
	}

	aceptarSolicitud() {
		const post = {
			action: '/mygroup/aceptar-solicitud',
			data: {
				'id': this.idGrupo,
			},
		};
		this.dataService.server(post).subscribe(
			res => {
				this.solicitudAceptada = res.success;
			},
			err => {
				return err;
			}
		);
	}

	preguntarCancelar() {

	}

  cancelarSolicitud() {

  }
}
