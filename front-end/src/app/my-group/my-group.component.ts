import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';

declare var $: any;

@Component({
	selector: 'app-my-group',
	templateUrl: './my-group.component.html',
	styleUrls: ['./my-group.component.css']
})

export class MyGroupComponent implements OnInit {

	solicitudesGrupos: any;
	gruposItems: any;
	token: string;

	constructor(public  dataService: DataService) {}

	ngOnInit() {
		this.getProfileData();
		$('.sidebar-left').removeClass('hide');
		$('.navbar-fixed').removeClass('hide');
	}

	loadGruposItems() {
		const post = {
			action: '/group',
			data: {}
		};
		this.dataService.server(post).subscribe(res => {
		  res.forEach( group => {
        const profilePhoto = group.profile_photo;
        if (profilePhoto == null || profilePhoto === '') {
          group.profile_photo = 'assets/img/guest.png';
        } else {
          // tslint:disable-next-line:max-line-length
          group.profile_photo  = this.dataService.url + '/group/cargarimagenperfil?archivo=' + profilePhoto + '&group_id=' + group.idGroup + '&auth_key=' + this.token;
        }
      });
      this.gruposItems = res;
		},
		err => {
			return err;
		});
	}

  getProfileData() {
    const post = {
      action: '/user/my-profile-data'
    };

    this.dataService.server(post).subscribe(res => {
        if (res.success) {
          this.token = res.data.auth_key;
          this.loadGruposItems();
        }
      },
      err => {
        return err;
      });

  }
}
