import {Component, EventEmitter, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../service/data.service';
import {UploadInput, UploadOutput} from 'ngx-uploader';

declare var $: any;

@Component({
  selector: 'app-config-group',
  templateUrl: './config-group.component.html',
  styleUrls: ['./config-group.component.css']
})
export class ConfigGroupComponent implements OnInit {

  token: string;
  idGroup: number;
  nameGroup: string;
  nameProfile: string;
  nameCover: string;

  loginAlert = 'El usuario y/o contraseña son incorrectos, Intente de nuevo' ;
  loginError = false;

  uploadInput: EventEmitter<UploadInput>;
  dragOver: boolean;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public dataService: DataService,
    public authenticationservice: DataService,
  ) {
    this.uploadInput = new EventEmitter<UploadInput>();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.idGroup = + params['idGroup'];
    });
    this.loadGroupData(this.idGroup);
    const that = this;
    $(document).ready(function() {
      $('#frm_configGroup').validate({
        errorClass: 'form-control-feedback',
        validClass: 'form-control-feedback',
        errorElement: 'span',
        highlight: function(element) {
          $(element).parents('div.form-group').addClass('has-danger').removeClass('has-success');
          $('.error').addClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-danger');
        },
        unhighlight: function(element) {
          $(element).parents('div.form-group').removeClass('has-danger').addClass('has-success');
          $('.error').removeClass('form-control-feedback');
          $(element).closest('input').addClass('form-control-success');
        },
        submitHandler: function(form) {
          that.updateGroup($(form).serializeFormJSON());
        }
      });
    });
  }

  loadGroupData(idGroup) {
    const post = {
      action: '/group/grupos',
      data: {}
    };
    this.dataService.server(post).subscribe(
      res => {
        res.forEach( group => {
          if (Number(group.idGroup) === idGroup) {
            this.nameGroup = group.nameGroup;
          }
        });
      },
      err => {
        return err;
      }
    );
  }

  updateGroup(form) {
    const post = {
      data: form,
      action: '/group/update'
    };
    this.authenticationservice.server(post).subscribe(res => {
        if (res.status === true) {
          this.loginError = false;
          window.location.replace('/groupDash/' + this.idGroup);
        } else {
          this.loginError = true;
          this.loginAlert = res.msg;
        }
      },
      err => {
        return err;
      });
  }

  onUploadProfileImage(output: UploadOutput) {
    switch (output.type) {
      case 'allAddedToQueue':
        const event: UploadInput = {
          type: 'uploadAll',
          // tslint:disable-next-line:max-line-length
          url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id=' + localStorage.getItem('usr_id'),
          method: 'POST',
          fieldName: 'UploadForm[images]'
        };
        this.uploadInput.emit(event);
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined' ) {
          this.nameProfile = output.file.name;
        }
        break;
      case 'uploading':
        break;
      case 'removed':
        this.nameProfile = '';
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
        break;
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        break;
      case 'rejected':
        break;
    }
  }

  onUploadCoverImage(output: UploadOutput) {
    switch (output.type) {
      case 'allAddedToQueue':
        const event: UploadInput = {
          type: 'uploadAll',
          // tslint:disable-next-line:max-line-length
          url: this.dataService.url + '/post/precarga?auth_key=' + localStorage.getItem('token') + '&usr_id=' + localStorage.getItem('usr_id'),
          method: 'POST',
          fieldName: 'UploadForm[images]'
        };
        this.uploadInput.emit(event);
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined' ) {
          this.nameCover = output.file.name;
        }
        break;
      case 'uploading':
        break;
      case 'removed':
        this.nameCover = '';
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
        break;
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        break;
      case 'rejected':
        break;
    }
  }

}
