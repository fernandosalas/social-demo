<?php

namespace app\controllers;

use Yii;
use app\models\Like;
use app\models\LikeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;

/**
 * LikeController implements the CRUD actions for Like model.
 */
class LikeController extends Controller
{
    /**
     * {@inheritdoc}
     */
/* ===================================== ALLOW ACCESS ORIGIN ================================================ */

public static function allowedDomains() {
   
        return ['*'];

}        


public function beforeAction($action) { 


    $this->enableCsrfValidation = false; 

    
    return parent::beforeAction($action); 

}


    public function init(){
        

            parent::init();
            
            \Yii::$app->user->enableSession = false;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        }
    
    public function behaviors(){
    
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create'],
                'rules' => [
                    [
                        'actions' => ['create','index','check'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                 'tokenParam' => 'auth_key'
            ],

             'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:i
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST','GET'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],
        ]; 
    }

/* ========================================= // ALLOW ACCESS ORIGIN ============================================ */

    /**
     * Lists all Like models.
     * @return mixed
     */
    public function actionIndex()
    {
        $likes = Like::find()->where(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type']  ])->asArray()->all();
       return [ 'likes'  => $likes ];
    }

  public function actionCheck()
    {
        echo $_POST['type'];
        $like = Like::findOne(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ]);

        $value = count($like) ? $like->value : 0 ;

        $prom = Like::find()->SELECT(['value' => 'AVG(value)'  ])
        ->WHERE(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ])
        ->groupBy(['id_target'])
        ->asArray()
        ->all();
       

        //validar type
      


        return [ 'value'  => (INT) $value, 'prom' => (INT) $prom[0]['value'] ];
    
    
    }

    /**
     * Displays a single Like model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Like model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //Relizao la busqueda del elemento para saber si existe , removi el value en este caso debido a que lo estamos buscando por ID
        $success = false;
        $model = Like::find()->where(['like_by'=> \Yii::$app->user->identity->id, 'isActive' => 1, 'id_target' => $_POST['Like']['id_target'], 'type' => $_POST['Like']['type'] ])->One();

    

        //atravez del conteo me doy cuenta si la cosulta regresa algun registro
        $exist = count($model) ? true : false ;

        //Verifico si existe
        if(!$exist){  

        //Creo los nuevos pasos para crear un nuevo elemento
        $model = new Like();
        $model->isActive = 1;
        $model->like_by = Yii::$app->user->identity->id; //extraigo el id del usuario con esta funcion de yii2




            if ($model->load(Yii::$app->request->post()) && $model->save()) { // cargo y valido la informacion && guardo el modelo
                 $success = true;
            }

        }else{
            //en caso de que exista uso el modelo de model para poder guardar,  
             $model->isActive = 1;
             $model->like_by = Yii::$app->user->identity->id; //extraigo el id del usuario con esta funcion de yii2
             if ($model->load(Yii::$app->request->post()) && $model->save()) {
                  $success = true;
                
             }

        }

        $prom = Like::find()->SELECT(['value' => 'AVG(value)'  ])
        ->WHERE(['isActive' => 1, 'id_target' => $_POST['Like']['id_target'] , 'type' => $_POST['Like']['type'] ])
        ->groupBy(['id_target'])
        ->asArray()
        ->all();
          

        return ['success' =>  $success , 'value' => (INT) $model->value, 'prom'=>(INT) $prom[0]['value'] ];

       
    }

    /**
     * Updates an existing Like model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_like]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Like model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Like model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Like the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Like::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
