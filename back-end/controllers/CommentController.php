<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use app\models\CommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /* ===================================== ALLOW ACCESS ORIGIN ================================================ */

    public static function allowedDomains() {

        return ['*'];

    }        


    public function beforeAction($action) { 


        $this->enableCsrfValidation = false; 

        
        return parent::beforeAction($action); 

    }


    public function init(){


        parent::init();

        \Yii::$app->user->enableSession = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    }
    
    public function behaviors(){

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','eliminar'],
                'rules' => [
                    [
                        'actions' => ['create','index', 'update','eliminar'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'tokenParam' => 'auth_key'
            ],

            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:i
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST','GET'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],
        ]; 
    }

    /* ========================================= // ALLOW ACCESS ORIGIN ============================================ */

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $comment =  new Comment();
        return $comment->getPostsCommets(\Yii::$app->user->identity->id, $_POST['id_target']);
        
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comment();
        $model->created_by = \Yii::$app->user->identity->id;
        $model->updated_by = \Yii::$app->user->identity->id;
        $model->isActive =  1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true , 'data'  =>  $model->getCommentData() ];
        }

        $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
        return [  'success' => false,'msg' =>  $message ];
   }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->created_by = \Yii::$app->user->identity->id;
        $model->updated_by = \Yii::$app->user->identity->id;
        $model->isActive =  1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           return ['success' => true , 'data'  =>  $model->getCommentData()[0] ];
       }

       return $model->load(Yii::$app->request->post());

       $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
       return [  'success' => false,'msg' =>  $message ];
   }

   public function actionRespond(){
        $model = new Comment();
        $model->insertResponse($_POST['id_target'],$_POST['content'],$_POST['type'],$_POST['created_by'],$_POST['isActive']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true , 'data'  =>  $model->getCommentData() ];
        }
        $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
        return [  'success' => false,'msg' =>  $message ];
   }

   public function actionEliminar($id)
   {


    $model = $this->findModel($id);


    $model->updated_by = \Yii::$app->user->identity->id;
    $model->isActive = 0;

    if ($model->save(false)) {
        return [  'success' => true];
    }


    $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
    return [  'success' => false,'msg' =>  $message,  'load' => $model->load( Yii::$app->request->post() ) ];
}

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne(['id_comment' => $id , 'created_by' => \Yii::$app->user->identity->id ])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
