<?php

namespace app\controllers;
use yii\filters\AccessControl;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use app\models\MyGroups;

class MygroupController extends \yii\web\Controller
{
	public static function allowedDomains() {

		return ['*'];

	}        


	public function beforeAction($action) 
	{ 

		$this->enableCsrfValidation = false; 


		return parent::beforeAction($action); 

	}


	public function init()
	{

		parent::init();
            \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //

        }
        
        public function behaviors()
        {
        	return [

        		'authenticator' => [
        			'class' => QueryParamAuth::className(),
        			'tokenParam' => 'auth_key'
        		],

        		'corsFilter'  => [
        			'class' => \yii\filters\Cors::className(),
        			'cors'  => [
                // restrict access to domains:i
        				'Origin'                           => static::allowedDomains(),
        				'Access-Control-Request-Method'    => ['POST','GET'],
        				'Access-Control-Allow-Credentials' => false,
                        'Access-Control-Max-Age'           => 3600, // Cache (seconds)
                    ],
                ],

                'verbs' => [
                	'class' => VerbFilter::className(),
                	'actions' => [
                		'logout' => ['post'],
                	],
                ],
            ]; 
        }
        public function actionIndex()
        {
           return $this->render('index');
       }
       public function actionAdmin()
       {
           $group = new MyGroups();
           $gruposAdministradosList =  $group->getGruposAdministrados(\Yii::$app->user->identity->id);
           return $gruposAdministradosList;

       // return $this->render('index');
       }
       public function actionMisGrupos()
       {
           $group = new MyGroups();
           $misGruposList =  $group->getMisGrupos(\Yii::$app->user->identity->id);
           return $misGruposList;

       // return $this->render('index');
       }
        public function actionUnir(){
           $model = new MyGroups();
           $model->idGroup = $_POST['idGroup'];
           $model->idUser = $_POST['idUsuario'];
           $model->actualizado_por = $_POST['idUsuario'];
           $model->isActive = 1 ;
           if($model->save()){
              $success = true;
           }else{
              $success = false;
           }

           return ['success'=> $success ];
        }


      public function actionEliminar($id){

         $model = $this->findModel($id);
         $model->actualizado_por = \Yii::$app->user->identity->id;
         $model->isActive = 0;
         if ( $model->save(false)) {

            return [ 'success' => true];

        }


        $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
        return [  'success' => false,'msg' =>  $message,  'load' => $model->load( Yii::$app->request->post() ) ];

    }


    public function actionAceptar(){
        $model = new MyGroups();
        return $model->aceptarSolicitud($_POST['idSolicitud'],$_POST['idUsuario']);
    }

    public function actionRechazar(){
        $model = new MyGroups();
        return $model->rechazarSolicitud($_POST['idSolicitud'],$_POST['idUsuario']);
    }


protected function findModel($id)
{ 
    if (($model = MyGroups::findOne(['idGroup' => $id, 'actualizado_por' => \Yii::$app->user->identity->id  ])) !== null) {
        return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
}

}
