<?php

namespace app\controllers;


use yii\filters\AccessControl;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use app\models\User;
use app\models\Like;
use app\models\Comment;
use app\models\UploadForm;
use app\models\Imagen;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */

    /* ===================================== ALLOW ACCESS ORIGIN ================================================ */

    public static function allowedDomains() {
       
        return ['*'];

    }        


    public function beforeAction($action) 
    { 

        $this->enableCsrfValidation = false; 

        
        return parent::beforeAction($action); 

    }


    public function init()
    {

        parent::init();
            \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //

   }
        
        public function behaviors()
        {
            return [
                    
                'authenticator' => [
                    'class' => QueryParamAuth::className(),
                    'tokenParam' => 'auth_key',
                    
                ],

                'corsFilter'  => [
                    'class' => \yii\filters\Cors::className(),
                    'cors'  => [
                // restrict access to domains:i
                        'Origin'                           => static::allowedDomains(),
                        'Access-Control-Request-Method'    => ['POST','GET'],
                        'Access-Control-Allow-Credentials' => false,
                        'Access-Control-Max-Age'           => 3600, // Cache (seconds)
            ],
        ],

        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ],
    ]; 
}

/* ========================================= // ALLOW ACCESS ORIGIN ============================================ */

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $post = new Post();
        if (isset($_POST['idUsuario'])){
            $postList =  $post->getProfilePosts($_POST['idUsuario']);
            foreach ($postList as $key => $post) {
                $like = Like::findOne(['isActive' => 1, 'id_target' =>  $postList[$key]['post_id'] , 'type' => 'post' ]);
                $value =  count(array($like)) ? $like->value : 0 ;
                    
                $prom = Like::find()->SELECT(['prom' => 'AVG(value)'  ])
                ->WHERE(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ])
                ->groupBy(['id_target'])
                ->asArray()
                ->all();
                
                $comment =  new Comment();
                $comments = $comment->getPostsComments($_POST['idUsuario'], $postList[$key]['post_id']);
                
                foreach($comments as $llave => $element){
                    $comments[$llave]['responses'] = $comment->getResponseComments($element['id_comment']);
                }
    
                $postList[$key]['like'] = $value;
                $postList[$key]['prom'] = $prom['prom'];
                $postList[$key]['comments'] = $comments;
    
                $model = new Imagen();
                $postList[$key]['imagenes'] = $model->galleria_post($postList[$key]['post_id'],$_GET['auth_key']);
            }
        }
        if (isset($_POST['idGroup'])){
            $postList =  $post->getGroupPosts($_POST['idGroup']);
            foreach ($postList as $key => $post) {
                $like = Like::findOne(['isActive' => 1, 'id_target' =>  $postList[$key]['post_id'] , 'type' => 'post' ]);
                $value = count($like) ? $like->value : 0 ;
    
                $prom = Like::find()->SELECT(['prom' => 'AVG(value)'  ])
                ->WHERE(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ])
                ->groupBy(['id_target'])
                ->asArray()
                ->all();
    
                $comment =  new Comment();
                $comments = $comment->getGroupPostsComments($postList[$key]['post_id']);
                
                foreach($comments as $llave => $element){
                    $comments[$llave]['responses'] = $comment->getResponseComments($element['id_comment']);
                }
    
                $postList[$key]['like'] = $value;
                $postList[$key]['prom'] = $prom['prom'];
                $postList[$key]['comments'] = $comments;
    
                $model = new Imagen();
                $postList[$key]['imagenes'] = $model->galleria_post($postList[$key]['post_id'],$_GET['auth_key']);
            }
        }

        return $postList;

    }

    public function actionUltimas(){
        $post = new Post();
        $postList =  $post->getHomePosts($_POST['idUsuario']);

        foreach ($postList as $key => $post) {

            $like = Like::findOne(['isActive' => 1, 'id_target' =>  $postList[$key]['post_id'] , 'type' => 'post' ]);
            $value = count($like) ? $like->value : 0 ;

            $prom = Like::find()->SELECT(['prom' => 'AVG(value)'  ])
            ->WHERE(['isActive' => 1, 'id_target' => $postList[$key]['post_id'] , 'type' => $_POST['type'] ])
            ->groupBy(['id_target'])
            ->asArray()
            ->all();

            $comment =  new Comment();
            $comments = $comment->getHomePostsComments($postList[$key]['post_id']);

            foreach($comments as $llave => $element){
                $comments[$llave]['responses'] = $comment->getResponseComments($element['id_comment']);
            }
            
            $postList[$key]['like'] = $value;
            $postList[$key]['prom'] = $prom['prom'];
            $postList[$key]['comments'] = $comments;

            $model = new Imagen();
            $postList[$key]['imagenes'] = $model->galleria_post($postList[$key]['post_id'],$_GET['auth_key']);

            
        }

        return $postList;
    }

    public function actionView($id){ 
       return  Post::findOne($id);
    }


    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){   
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON ;
        $model = new Post();
        $model->created_by = \Yii::$app->user->identity->id;
        $model->updated_by = \Yii::$app->user->identity->id;
        $model->created_by_group = $_POST['created_by_group'];
        $model->wall_target = $_POST['wall_target'];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($_POST['imagenesPost'] != ''){
                $imagenes =  explode(',', $_POST['imagenesPost'] );
                $this->moverImagenes( $imagenes ,$model->post_id);
            }
            return ['success' => true, 'post'=> $model->getPostData() ];
        }
        $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
        return [  'success' => false,'msg' =>  $message ];
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(){
        $model = new Post();
        $model->updatePost($_POST['post_id'],$_POST['post_content'],$_POST['updated_by'],$_POST['updated_at']);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return  [
                'status' => true , 
                'token'  =>  $model->auth_key, 
                'username'  =>  $model->username,
                'usr_id'  => \Yii::$app->user->identity->id
            ];
        }
    }

    public function actionEliminar($id)
    {
        
        $model = $this->findModel($id);

        $model->updated_by = \Yii::$app->user->identity->id;
        $model->active = 0;
        
        if ( $model->save(false)) {
            return [  'success' => true];
        }

        $message = implode(' ', array_map(function ($errors) { return implode(' ', $errors);}, $model->getErrors() ));
        return [  'success' => false,'msg' =>  $message,  'load' => $model->load( Yii::$app->request->post() ) ];
    }


    protected function moverImagenes($imagenes,$post_id){

        if(!count($imagenes)){
            return true;
        }

        $userID = \Yii::$app->user->identity->id;
        $ds = DIRECTORY_SEPARATOR;
        $userDir = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$userID.$ds.'posts' . $ds . $post_id .$ds  ;  
        //Crear el dictorio si no existe
        if (!file_exists($userDir)) {
            mkdir($userDir , 0755, true);
        }

        $tempDir = \Yii::$app->basePath. $ds .'storage'.$ds.'tmp'. $ds;  
        
        foreach ($imagenes as $key => $archivo_imagen) {
            /*Mover Imagen Original */
            $ubicacionTemporal_imagen= $tempDir . 'Temp'. $userID. "_" . $archivo_imagen;   
            $nuevaUbicacion_imagen =  $userDir . $archivo_imagen;
            rename(  $ubicacionTemporal_imagen, $nuevaUbicacion_imagen);
            /*Mover miniatura*/    
            $ubicacionTemporal_miniatura = $tempDir . 'Temp_thumb'. $userID. "_" . $archivo_imagen; 
            $nuevaUbicacion_miniatura = $userDir . 'thumb_'. $userID. "_" . $archivo_imagen;
            rename($ubicacionTemporal_miniatura  ,  $nuevaUbicacion_miniatura );
            //guardar_imagen en la base de datos
                    
            $model = new Imagen();
            $model->insertar_registro($archivo_imagen, $key , 'post', $post_id);

        }

    }



   public function actionPrecarga(){
        //return $_FILES;
        $model = new UploadForm();

        if (Yii::$app->request->isPost ) {

            //return $_FILES;

            $model->images = UploadedFile::getInstance($model, 'images');

            if($model->validate()) { 

                $imagenTemporal = $model->uploadTemp();
                return ['archivo' => $imagenTemporal ];
            }else{

                throw new \yii\web\NotFoundHttpException($model->getErrors()['images'][0]);

            }
        }

       
    }

   public function actionCargarimagen($file){
        try { 

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
            
            $userID = \Yii::$app->user->identity->id;
            $tempDir = \Yii::$app->basePath. DIRECTORY_SEPARATOR."storage/tmp/";  
            $loadfile = $tempDir . 'Temp_thumb'. $userID. "_" . $file;  
            //
            $width =  50;
            $height = 50;
            $type = IMAGETYPE_JPEG;
           
           
            $img_data = file_get_contents($loadfile, 'r');
    

        } catch (Exception $e) {
               \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
               return  [$e->getMessage()];
        }
         Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
         Yii::$app->response->data = $img_data;
         
    }  
    

     public function actionCargarImagenCompleta($archivo,$post_id, $usr_id){
        try { 
            $ds = DIRECTORY_SEPARATOR;
            $ruta_archivo = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$usr_id. $ds.'posts' . $ds . $post_id .$ds . $archivo ;  
            if(file_exists($ruta_archivo)){ 
                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
                $img_data = file_get_contents($ruta_archivo, 'r');
            }else{
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
                return  ['error' => true ];  
            }    

        } catch (Exception $e) {
               \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
               return  [$e->getMessage()];
        }

         Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
         Yii::$app->response->data = $img_data;
    }

    public function actionCargarimagenperfil($usr_id){
      
        try {
            $user = new User;
            $archivo = $user->getImagenPerfil($usr_id);
            $ds = DIRECTORY_SEPARATOR;
            $ruta_archivo = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$usr_id. $ds.'profile' .$ds . $archivo;  
            if($archivo =="" || (!file_exists($ruta_archivo))) {  
             $ruta_archivo = \Yii::$app->basePath. $ds . 'web' .$ds.'img' .$ds. 'default_profile.png';
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
            $img_data = file_get_contents($ruta_archivo, 'r'); 
            $type = IMAGETYPE_JPEG;
            Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
            Yii::$app->response->data = $img_data;

        } catch (Exception $e) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; // 
            return  [$e->getMessage()];
        }
       

        
    }

    public function actionCargarimagenportada($usr_id){

            try {
                $user = new User;
                $archivo = $user->getImagenPortada($usr_id);
                $ds = DIRECTORY_SEPARATOR;
                $ruta_archivo = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$usr_id. $ds.'profile' .$ds . $archivo;
                if($archivo =="" || (!file_exists($ruta_archivo))) {
                 $ruta_archivo = \Yii::$app->basePath. $ds . 'web' .$ds.'img' .$ds. 'default_profile.png';
                }
                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
                $img_data = file_get_contents($ruta_archivo, 'r');
                $type = IMAGETYPE_JPEG;
                Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
                Yii::$app->response->data = $img_data;

            } catch (Exception $e) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
                return  [$e->getMessage()];
            }



        }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    { 
        if (($model = Post::findOne(['post_id' => $id, ' ' => \Yii::$app->user->identity->id  ])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

 


    public function  getPostDirectory($post_id){
        $userID =  \Yii::$app->user->identity->id;
        return  'uploads/user_' .  $userID .'/posts/post_' . $post_id . '/';
    }
    

}
