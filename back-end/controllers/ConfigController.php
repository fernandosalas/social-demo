<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;

class ConfigController extends \yii\web\Controller
{

	public static function allowedDomains() {
       
        return ['*'];

    }        


    public function beforeAction($action) { 


        $this->enableCsrfValidation = false; 

        
        return parent::beforeAction($action); 

    }


    public function init(){
        

        parent::init();
        
        \Yii::$app->user->enableSession = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    }
    
    public function behaviors(){
        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','update'],
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'tokenParam' => 'auth_key'
            ],

            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:i
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST','GET'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],
        ]; 
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpdate(){ 

        $model = User::findOne(['usr_id' => \Yii::$app->user->identity->id ]); 
        $model->scenario = 'editUser';
        $userID = \Yii::$app->user->identity->id;
        $ds = DIRECTORY_SEPARATOR;
        if($_POST['profile_img'] || $_POST['cover_img']){

            $userDir = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$userID.$ds.'profile' . $ds;  
            //Crear el dictorio si no existe
            if (!file_exists($userDir)) {
                mkdir($userDir , 0755, true);
            }
            $tempDir = \Yii::$app->basePath. $ds .'storage'.$ds.'tmp'. $ds;
        }

        if($_POST['profile_img']){
            /*Mover Imagen Original */
            $prof_ubicacionTemporal_imagen = $tempDir . 'Temp'. $userID. "_" . $_POST['profile_img'];   
            $prof_nuevaUbicacion_imagen =  $userDir . $_POST['profile_img'];

            if(!file_exists($prof_ubicacionTemporal_imagen)){
              return ['status' => false,'msg' =>  'Ocurrio un error al guardar el perfil 1 ' . $prof_nuevaUbicacion_imagen ];      
            }

            copy($prof_ubicacionTemporal_imagen, $prof_nuevaUbicacion_imagen);
            /*Mover miniatura*/    
            $prof_ubicacionTemporal_miniatura = $tempDir . 'Temp_thumb'. $userID. "_" . $_POST['profile_img']; 

            if(!file_exists($prof_ubicacionTemporal_miniatura)){
              return ['status' => false,'msg' =>  'Ocurrio un error al guardar el perfil 2'];      
            } 

            $prof_nuevaUbicacion_miniatura = $userDir . 'thumb_'. $userID. "_" . $_POST['profile_img'];
            copy($prof_ubicacionTemporal_miniatura  ,  $prof_nuevaUbicacion_miniatura );
            //guardar_imagen en la base de datos
            $model->profile_photo = $_POST['profile_img'] ;
        }

        if($_POST['cover_img']){
            /*Mover Imagen Original */
            $cover_ubicacionTemporal_imagen = $tempDir . 'Temp'. $userID. "_" . $_POST['cover_img'];   
            $nuevaUbicacion_imagen =  $userDir . $_POST['cover_img'];

            if(!file_exists($cover_ubicacionTemporal_imagen)){
                  return [
                   'status' => false,
                   'msg' => 'Ocurrio un error al guardar el perfil 3',
                   'path' => $ubicacionTemporal_imagen 
                ];  
            }



            copy($cover_ubicacionTemporal_imagen, $nuevaUbicacion_imagen);
            /*Mover miniatura*/    
            $cover_ubicacionTemporal_miniatura = $tempDir . 'Temp_thumb'. $userID. "_" . $_POST['cover_img']; 


            if(!file_exists($cover_ubicacionTemporal_miniatura)){
              return ['status' => false,'msg' => 'Ocurrio un error al guardar el perfil 4'];      
            }


            $cover_nuevaUbicacion_miniatura = $userDir . 'thumb_'. $userID. "_" . $_POST['cover_img'];
            copy ($cover_ubicacionTemporal_miniatura  ,  $cover_nuevaUbicacion_miniatura );
            //guardar_imagen en la base de datos
            $model->cover_photo = $_POST['cover_img'] ;
        }

        

        $model->country = $_POST['country'];
        $model->city = $_POST['city'];


        if($model->save()){
             if($_POST['profile_img']){
                unlink($prof_ubicacionTemporal_imagen);
                unlink($prof_nuevaUbicacion_miniatura);
            }
            if($_POST['cover_img']){ 
                unlink($cover_ubicacionTemporal_imagen);
                unlink($cover_nuevaUbicacion_miniatura);
            }

         return  [
             'status' => true , 
             'token'  =>  $model->auth_key, 
             'username'  =>  $model->username,
             'usr_id'  => \Yii::$app->user->identity->id
         ];
     }

     $message = implode(' ', array_map(function ($errors) {return implode(' ', $errors);}, $model->getErrors()));
     
     
     
     return [  'status' => false,'msg' =>  $message ];
 }

}

/*protected function findModel($id)
    {
        if (($model = Post::findOne(['usr_id' => \Yii::$app->user->identity->id  ])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }*/