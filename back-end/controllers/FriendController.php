<?php

namespace app\controllers;

use yii\filters\AccessControl;
use Yii;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use app\models\User;
use app\models\Friends;
use app\models\Notification;




class FriendController extends \yii\web\Controller
{//-
	public static function allowedDomains() {

		return ['*'];

	}        


	public function beforeAction($action) 
	{ 

		$this->enableCsrfValidation = false; 
		return parent::beforeAction($action); 

	}


	public function init()
	{

		parent::init();
            \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //

        }
        
        public function behaviors()
        {
            return [

                'authenticator' => [
                    'class' => QueryParamAuth::className(),
                    'tokenParam' => 'auth_key'
                ],

                'corsFilter'  => [
                    'class' => \yii\filters\Cors::className(),
                    'cors'  => [
                // restrict access to domains:i
                        'Origin'                           => static::allowedDomains(),
                        'Access-Control-Request-Method'    => ['POST','GET'],
                        'Access-Control-Allow-Credentials' => false,
                        'Access-Control-Max-Age'           => 3600, // Cache (seconds)
                    ],
                ],

                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ]; 
        }
        
        public function actionIndex(){
        	$friend = new Friends();
        	$friendList =  $friend->getProfileFriends($_POST['idUsuario']);
        	return $friendList;
        }

        public function actionSugerencias(){
            $addFriend = new Friends();
            return $addFriend->getProfileAddFriends(\Yii::$app->user->identity->id);
        }

        public function actionSolicitudes(){ 
            $addFriend = new Friends();
            return $addFriend->getSolicitudesAmistad(\Yii::$app->user->identity->id);
        }

        public function actionView($id){ 
        	return  Friends::findOne($id);
        }

        public function actionAgregar(){
            $model = new Friends();
            $notification = new Notification;
            $success = false;
            $friend = User::findOne($_POST['idUsuario']);
            if($model->guardarSolicitud($_POST['idUsuario'],$_POST['idFriend'])){
                $notification->insertNotification(
                    $friend->full_name . ' te ha enviado una solicitud de amistad',
                    'solicitud de amistad',
                    'buscaramigos',
                     $_POST['idFriend'],
                     $_POST['idUsuario']
                    
                );
                $success =  true;
            }
           

            return ['success'=> $success ];
        }

        public function actionAceptar(){
            $model = new Friends();
            return ['success' => $model->aceptarSolicitud($_POST['idSolicitud'],$_POST['idUsuario']) ];
        }

        public function actionRechazar(){
            $model = new Friends();
            return ['success' => $model->rechazarSolicitud($_POST['idSolicitud'],$_POST['idUsuario'])];
        }

        protected function findModel($id){ 
            if (($model = Friends::findOne(['idFriend' => $id, 'actualizado_por' => \Yii::$app->user->identity->id  ])) !== null) {
                return $model;
            }
            throw new NotFoundHttpException('The requested page does not exist.');
        }
}