<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\ContactForm;
use yii\filters\auth\QueryParamAuth;

class SiteController extends Controller
{
    /** 
     * {@inheritdoc}
     */ 

    /* ===================================== ALLOW ACCESS ORIGIN ================================================ */

    public static function allowedDomains() {
            return ['*','localhost'];
    }

     public function beforeAction($action){
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
     }

   public function behaviors(){
        return [
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'tokenParam' => 'auth_key',
                'except' => ['login','registrar']
            ],

            'corsFilter'  => [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
            //restrict access to domains:i
                'Origin' => ['*'],
                'Access-Control-Request-Method'    => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Max-Age'           => 3600, // Cache (seconds)
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ]; 
    }
 
/* ========================================= // ALLOW ACCESS ORIGIN ============================================ */


    /**
     * Displays homepage.
     *
     * @return string 
     */
    public function actionIndex(){
        
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new LoginForm();
        $model->username = $_POST['username'];
        $model->password = $_POST['password'];
        if($_POST['remember'] == 1){
            $model->rememberMe = true;
        }else{
            $model->rememberMe = false;
        }
        if($model->validate()) {
            $model->password = '';
            return  ['status' => true, 'token' => $model->generateToken(), 'usr_id'  => $model->getUserID(), 'usr_data'  => $model->getUser() ]; 
        }else{
            $model->password = '';
            return [ 'status' => false , 'errors' => $model->getErrors() ] ;
        }

    }




    public function actionRegistrar(){

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = New User; 
        $model->scenario = 'register';
       
        
        $user = $model->find()->where(["username" => $model->username])->one();

        // $email=Yii::$app->request->post['email'];
        //  $request=Yii::$app->request->post('User')['username'];
        //  $email = $request->post('user-username');
        //echo "$email";

        //$subject = "Confirmar registro";
        //$body = "<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>";
        // $body .= "<a href='http://yii.local/index.php?r=site/confirm&id=".$id."&authKey=".$authKey."'>Confirmar</a>";
        //$body .= "<a href='http://app.galloreal.com'>Confirmar</a>";

       /* //Enviamos el correo
        Yii::$app->mailer->compose()
        //->setTo('blaerius168cks@gmail.com')
        ->setTo($model->username)
        ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
        ->setSubject($subject)
        ->setHtmlBody($body)
        //->send();
        */

        if($model->load(Yii::$app->request->post()) && $model->save()){
         return  [
             'status' => true , 
             'token'  =>  $model->auth_key, 
             'email'  =>  $model->username,
             'usr_id'  => $model->usr_id
         ];


     }

     $message = implode(' ', array_map(function ($errors) {return implode(' ', $errors);}, $model->getErrors()));
     return [  'status' => false,'msg' =>  $message ];


 }


    /**
     * Logout action.         
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
    }


}
