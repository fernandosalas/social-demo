<?php

namespace app\controllers;

use yii\filters\AccessControl;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use app\models\Group;
use  app\models\MyGroups;
class GroupController extends \yii\web\Controller
{
	public static function allowedDomains() {

		return ['*'];

	}        


	public function beforeAction($action) 
	{ 

		$this->enableCsrfValidation = false; 


		return parent::beforeAction($action); 

	}


	public function init()
	{

		parent::init();
            \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //

        }
        
        public function behaviors()
        {
        	return [

        		'authenticator' => [
        			'class' => QueryParamAuth::className(),
        			'tokenParam' => 'auth_key'
        		],

        		'corsFilter'  => [
        			'class' => \yii\filters\Cors::className(),
        			'cors'  => [
                // restrict access to domains:i
        				'Origin'                           => static::allowedDomains(),
        				'Access-Control-Request-Method'    => ['POST','GET'],
        				'Access-Control-Allow-Credentials' => false,
                        'Access-Control-Max-Age'           => 3600, // Cache (seconds)
                    ],
                ],

                'verbs' => [
                	'class' => VerbFilter::className(),
                	'actions' => [
                		'logout' => ['post'],
                	],
                ],
            ]; 
        }
        public function actionIndex()
        {
    	//mostrar grupos sugeridos
        	$group = new Group();
        	$gruposSugeridosList =  $group->getGruposSugeridos(\Yii::$app->user->identity->id);
        	return $gruposSugeridosList;

        // return $this->render('index');
        }

        public function actionGet(){
            $group = new Group();
            return $group->getGrupo($_POST['idGroup']);
        }

        public function actionSolicitudes(){
					$group = new Group();
					return $group->getSolicitudesGrupos($_POST['idGroup']);
				}
				
				public function actionMiembros(){
					$group = new Group();
					return $group->getMiembrosGrupos($_POST['idGroup']);
        }

        public function actionAdmin()
        {
        	$group = new Group();
        	$gruposAdministradosList =  $group->getGruposAdministrados(\Yii::$app->user->identity->id);
        	return $gruposAdministradosList;

       // return $this->render('index');
        }
        public function actionGrupos()
        {
        	$group = new Group();
        	$misGruposList =  $group->getMisGrupos(\Yii::$app->user->identity->id);
        	return $misGruposList;
        }
       
        public function actionCrear(){
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	$model = New Group; 
        	$model->scenario = 'nuevogrupo';
            $MyGroups = new MyGroups();

        	if( $model->load(Yii::$app->request->post()) && $model->save() ){

                     $MyGroups->idGroup = $model->idGroup;
                     $MyGroups->idUser  = $model->createdBy;
                     $MyGroups->isAdmin = 1 ; 
                     $MyGroups->actualizado_por = $model->createdBy ; 
                     $MyGroups->autorizado_por  = $model->createdBy ;
                     $MyGroups->aceptada  = 1;
                     $MyGroups->rechazada = 0;
                     $MyGroups->isActive  = 1;
                     $MyGroups->save();
                     if( $MyGroups->save() ){ 
                        
                		return  [
                			 'status'     =>  true,
                             'data'       =>  $model,
                		];

                }else{
                    
                $message = implode( ' ', array_map(function ($errors) { return implode(' ', $errors); }, $MyGroups->getErrors() ) );
                    return [  'status' => false,'msg' =>  $message ];
                    
                }
        	}

        	$message = implode(' ', array_map(function ($errors) {return implode(' ', $errors);}, $model->getErrors()));
        	return [  'status' => false,'msg' =>  $message ];

        }

        public function actionUpdate(){
            $userID = \Yii::$app->user->identity->id;
            if($_POST['profile_img'] || $_POST['cover_img']){
                $ds = DIRECTORY_SEPARATOR;
                $userDir = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$userID.$ds.'groups' . $ds;
                //Crear el dictorio si no existe
                if (!file_exists($userDir)) {
                    mkdir($userDir , 0755, true);
                }
                $tempDir = \Yii::$app->basePath. $ds .'storage'.$ds.'tmp'. $ds;
            }
            if($_POST['profile_img']){
                /*Mover Imagen Original */
                $ubicacionTemporal_imagen= $tempDir . 'Temp'. $userID. "_" . $_POST['profile_img'];
                $nuevaUbicacion_imagen =  $userDir . $_POST['profile_img'];
                rename(  $ubicacionTemporal_imagen, $nuevaUbicacion_imagen);
                /*Mover miniatura*/
                $ubicacionTemporal_miniatura = $tempDir . 'Temp_thumb'. $userID. "_" . $_POST['profile_img'];
                $nuevaUbicacion_miniatura = $userDir . 'thumb_'. $userID. "_" . $_POST['profile_img'];
                rename($ubicacionTemporal_miniatura  ,  $nuevaUbicacion_miniatura );
                //guardar_imagen en la base de datos
                $model = new Group();
                $model->updateProfilePhoto($_POST['idGroup'],$_POST['profile_img']);
            }
            if($_POST['cover_img']){
                /*Mover Imagen Original */
                $ubicacionTemporal_imagen= $tempDir . 'Temp'. $userID. "_" . $_POST['cover_img'];
                $nuevaUbicacion_imagen =  $userDir . $_POST['cover_img'];
                rename(  $ubicacionTemporal_imagen, $nuevaUbicacion_imagen);
                /*Mover miniatura*/
                $ubicacionTemporal_miniatura = $tempDir . 'Temp_thumb'. $userID. "_" . $_POST['cover_img'];
                $nuevaUbicacion_miniatura = $userDir . 'thumb_'. $userID. "_" . $_POST['cover_img'];
                rename($ubicacionTemporal_miniatura  ,  $nuevaUbicacion_miniatura );
                //guardar_imagen en la base de datos
                $model = new Group();
                $model->updateCoverPhoto($_POST['idGroup'],$_POST['cover_img']);
            }
            $model = new Group();
            $model->updateGroup($_POST['idGroup'],$_POST['nameGroup']);
            return  [
                'status' => true,
                'id_group'  =>  $model->idGroup,
                'name_group'  =>  $model->nameGroup
            ];
        }

        public function actionCargarimagenperfil($group_id){
                try {
                    $group = new Group;
                    $archivo = $group->getImagenPerfil($group_id);
                    $ds = DIRECTORY_SEPARATOR;
                    $userID = \Yii::$app->user->identity->id;
                    $ruta_archivo = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$userID. $ds.'groups' .$ds . $archivo;
                    if($archivo =="" || (!file_exists($ruta_archivo))) {
                     $ruta_archivo = \Yii::$app->basePath. $ds . 'web' .$ds.'img' .$ds. 'default_profile.png';
                    }
                    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
                    $img_data = file_get_contents($ruta_archivo, 'r');
                    $type = IMAGETYPE_JPEG;
                    Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
                    Yii::$app->response->data = $img_data;
                } catch (Exception $e) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
                    return  [$e->getMessage()];
                }
        }

        public function actionCargarimagenportada($group_id){
                try {
                    $group = new Group;
                    $archivo = $group->getImagenPortada($group_id);
                    $ds = DIRECTORY_SEPARATOR;
                    $userID = \Yii::$app->user->identity->id;
                    $ruta_archivo = \Yii::$app->basePath. $ds.'storage'.$ds.'users'.$ds.'usr_'.$userID. $ds.'groups' .$ds . $archivo;
                    if($archivo =="" || (!file_exists($ruta_archivo))) {
                     $ruta_archivo = \Yii::$app->basePath. $ds . 'web' .$ds.'img' .$ds. 'default_profile.png';
                    }
                    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW ;
                    $img_data = file_get_contents($ruta_archivo, 'r');
                    $type = IMAGETYPE_JPEG;
                    Yii::$app->response->headers->add('content-type', image_type_to_mime_type($type));
                    Yii::$app->response->data = $img_data;
                } catch (Exception $e) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
                    return  [$e->getMessage()];
                }
        }
}
