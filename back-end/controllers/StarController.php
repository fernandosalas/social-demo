<?php

namespace app\controllers;

use Yii;
use app\models\Star;
use app\models\StarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;

/**
 * LikeController implements the CRUD actions for Like model.
 */
class StarController extends Controller
{
    /**
     * {@inheritdoc}
     */
/* ===================================== ALLOW ACCESS ORIGIN ================================================ */

public static function allowedDomains() {
   
        return ['*'];

}        


public function beforeAction($action) { 


    $this->enableCsrfValidation = false; 

    
    return parent::beforeAction($action); 

}


    public function init(){
        

            parent::init();
            
            \Yii::$app->user->enableSession = false;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        }
    
    public function behaviors(){
    
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create'],
                'rules' => [
                    [
                        'actions' => ['create','index','check'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                 'tokenParam' => 'auth_key'
            ],

             'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:i
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST','GET'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],
        ]; 
    }

/* ========================================= // ALLOW ACCESS ORIGIN ============================================ */

    /**
     * Lists all Like models.
     * @return mixed
     */
    public function actionIndex()
    {
        $stars = Star::find()->where(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ])->asArray()->all();
       return [ 'stars'  => $stars ];
    }

  public function actionCheck()
    {
        $stars = Star::find()->where(['isActive' => 1, 'id_target' => $_POST['id_target'] , 'type' => $_POST['type'] ])->asArray()->all();
        $isStar = count($stars) ? true : false;
        return [ 'star'  =>  $isStar];
    }

    /**
     * Displays a single Star model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Star model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $stars = Star::find()->where(['star_by'=> \Yii::$app->user->identity->id, 'isActive' => 1, 'id_target' => $_POST['Star']['id_target'], 'type' => $_POST['Star']['type'] ])->all();

        $disStar = count($stars) ? true : false ;


        if(!$disStar){  

        $model = new Star();
        $model->isActive = 1;
        $model->star_by = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 $isStar = true;
            }else{
                 $isStar = false;
            }



        
        }else{

        Star::deleteAll(['star_by'=> \Yii::$app->user->identity->id, 'isActive' => 1, 'id_target' => $_POST['Star']['id_target'], 'type' => $_POST['Star']['type'] ]);

        $isStar = false;

        }

        $stars = Star::find()->where(['star_by'=> \Yii::$app->user->identity->id, 'isActive' => 1, 'id_target' =>$_POST['Star']['id_target'] , 'type' => $_POST['Star']['type'] ])->asArray()->all();


        return ['success' => true , 'star' => $isStar, 'stars'  => $stars ];

       
    }

    /**
     * Updates an existing Like model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_star]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Like model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Like model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Like the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Star::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
