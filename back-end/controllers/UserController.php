<?php

namespace app\controllers;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use app\models\User;

class UserController extends \yii\web\Controller
{   
    /* ===================================== ALLOW ACCESS ORIGIN ================================================ */

    public static function allowedDomains() {
        return ['*'];
    }        

    public function beforeAction($action) { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    
    public function init(){
        parent::init();
        \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
        }

    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    } 
            
    public function behaviors(){
        return [  
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'tokenParam' => 'auth_key',
                
            ],
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                // restrict access to domains:i
                'Origin'                           => static::allowedDomains(),
                'Access-Control-Request-Method' => ['GET', 'POST'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Max-Age'           => 3600, // Cache (seconds),
                'response' => 200
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ]; 
    }
    
  
    /* ========================================= // ALLOW ACCESS ORIGIN ============================================ */

    public function actionIndex()
    {
        return $this->render('index');
    }
 
    function actionObtener(){
        $user = new User();
    	return $user->obtener($_POST['idUsuario']);
    }
 
    public function actionMyProfileData(){
        $user = new User();
        $currentUser =  \Yii::$app->user->identity->id;
        $user = $user->findIdentity($currentUser); 
        return  $user ? [ 'success' => true, 'id' => \Yii::$app->user->identity->id , 'data' =>  $user ]: ['success' => false  ];
     }

}
