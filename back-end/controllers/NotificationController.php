<?php

namespace app\controllers;

use Yii;
use app\models\Notification;
use app\models\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;
use yii\filters\AccessControl;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */

    /* ===================================== ALLOW ACCESS ORIGIN ================================================ */

    public static function allowedDomains() {
       
        return ['*'];

    }        


    public function beforeAction($action) 
    { 

        $this->enableCsrfValidation = false; 

        
        return parent::beforeAction($action); 

    }


    public function init(){
        parent::init();
            \Yii::$app->user->enableSession = false; //Desactiva la sesion para activar la autenticación por token
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(){
                return [
                        
                    'authenticator' => [
                        'class' => QueryParamAuth::className(),
                        'tokenParam' => 'auth_key',
                        
                    ],

                    'corsFilter'  => [
                        'class' => \yii\filters\Cors::className(),
                        'cors'  => [
                    // restrict access to domains:i
                            'Origin'                           => static::allowedDomains(),
                            'Access-Control-Request-Method'    => ['POST','GET'],
                            'Access-Control-Allow-Credentials' => false,
                            'Access-Control-Max-Age'           => 3600, // Cache (seconds)
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ]; 
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex(){
        $model = new Notification();
        return $model->getNotifications($_POST['idUsuario']);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Notification();
        return ['success'=>$model->insertNotification(
            $_POST['text'],
            $_POST['type'],
            $_POST['link'],
            $_POST['created_by'],
            $_POST['user_target']
        )];
    }

    public function actionRead(){
        $model = new Notification();
        return $model->markAsRead($_POST['idNotificacion']);
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_notification]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */



    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



}
