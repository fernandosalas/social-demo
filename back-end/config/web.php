<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
  

$config = [
    'id' => 'Galloreal',
    'basePath' => dirname(__DIR__),
    'language' => 'es',

    'bootstrap' => ['log'],
    'aliases' => [ 
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'response' => [
            'class' => 'yii\web\Response',
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
            'on beforeSend' => function (yii\base\Event $event) {
                $response = $event->sender;
                $headers = Yii::$app->response->headers;
                 
                if ( $response->statusCode !== 200 && is_array($response->data)) {

                    $headers->add('Access-Control-Allow-Origin', '*');
                    $headers->add('Access-Control-Allow-Credentials', true);
                    $headers->add('Access-Control-Max-Age', 3600);
                    
                    $response->data['code'] = $response->data['status'];
                    $response->data['status'] = 'Error';
                    unset($response->data['type']);
                    $response->statusCode = 200; 

                }
            },
      ],
      'request' => [
        'enableCookieValidation' => false,
        'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'cache' => [ 
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl'=> null,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
           /* 'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',
            'username' => 'ptest321123@gmail.com',
            'password' => '1234ABCD',
            'port' => '587',
            'encryption' => 'tls',
        ],*/
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'db' => $db,
     
    'urlManager' => [
        'class' => 'yii\web\UrlManager',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
           // 'transaction/getrequestdetail/<id>' => 'transaction/getrequestdetail',
        ],
    ],

    /*'log' => [
         'targets' => [
             [
                 'class' => 'yii\log\EmailTarget',
                 'mailer' => 'mailer',
                 'levels' => ['error'],
                 'message' => [
                     'from' => ['sistema@galloreal.com'],
                     'to' => ['fernando.salas@juancker.com'],
                     'subject' => 'Log Message',
                 ],
             ],
         ],
    ],*/

],
'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
