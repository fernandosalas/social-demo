<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "myGroups".
 *
 * @property int $idGroup
 * @property int $idUser
 * @property bool $isAdmin
 * @property int $actualizado_por
 * @property int $autorizado_por
 * @property bool $aceptada
 * @property bool $rechazada
 * @property int $idSolicitud
 * @property bool $isActive
 */
class MyGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'myGroups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actualizado_por', 'autorizado_por'], 'required'],
            [['idGroup', 'idUser', 'actualizado_por', 'autorizado_por', 'idSolicitud'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idGroup' => 'Id Group',
            'idUser' => 'Id User',
            'isAdmin' => 'Is Admin',
            'actualizado_por' => 'Actualizado Por',
            'autorizado_por' => 'Autorizado Por',
            'aceptada' => 'Aceptada',
            'rechazada' => 'Rechazada',
            'idSolicitud' => 'Id Solicitud',
            'isActive' => 'Is Active',
        ];
    }


     public function getIdGroup(){
        return $this->hasOne(Group::className(), ['idGroup' => 'idGroup']);
    }

    public function aceptarSolicitud($solicitud_id,$idUsuario){
        $request = MyGroups::findOne($solicitud_id);
        $request->aceptada = 1;
        $request->actualizado_por = $idUsuario;
        $request->autorizado_por = $idUsuario;
        $request->save();
    }

    public function rechazarSolicitud($solicitud_id,$idUsuario){
        $request = $this->findOne($solicitud_id);
        $request->rechazada = 1;
        $request->actualizado_por = $idUsuario;
        $request->autorizado_por = $idUsuario;
        $request->save();
    }
}
