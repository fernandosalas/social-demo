<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\City;

/**
 * CitySearch represents the model behind the search form of `app\models\City`.
 */
class CitySearch extends City
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_CITY'], 'integer'],
            [['CC_FIPS', 'CC_ISO', 'FULL_NAME_ND'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_CITY' => $this->ID_CITY,
        ]);

        $query->andFilterWhere(['like', 'CC_FIPS', $this->CC_FIPS])
            ->andFilterWhere(['like', 'CC_ISO', $this->CC_ISO])
            ->andFilterWhere(['like', 'FULL_NAME_ND', $this->FULL_NAME_ND]);

        return $dataProvider;
    }
}
