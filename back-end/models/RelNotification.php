<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rel_Notification".
 *
 * @property int $idNotification
 * @property int $idUser
 * @property string $Link
 * @property bool $isRead
 * @property string $read on
 */
class RelNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rel_Notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idNotification', 'idUser'], 'required'],
            [['idNotification', 'idUser'], 'integer'],
            [['isRead'], 'boolean'],
            [['
read on'], 'safe'],
            [['Link'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idNotification' => 'Id Notification',
            'idUser' => 'Id User',
            'Link' => 'Link',
            'isRead' => 'Is Read',
            'read on' => 'Read On',
        ];
    }
}
