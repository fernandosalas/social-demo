<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\base\Security;
use yii\web\IdentityInterface;


use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $usr_id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $genere
 * @property int $created_by
 * @property int $modified_by
 * @property string $created_at
 * @property string $last_login
 * @property string $modified_at
 * @property string $timezone
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    private $security ;
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_EDITUSER = 'editUser';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Users';
    }

    public static function primaryKey()
    {
        return ["usr_id"];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    function __construct(){

        $this->security = new Security();
    }


    public function scenarios()
    {
      $scenarios = parent::scenarios();
      $scenarios[self::SCENARIO_LOGIN] = ['username', 'password'];
      $scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password','full_name','country','city'];
      $scenarios[self::SCENARIO_EDITUSER] = ['username', 'email','password','full_name','profile_img','cover_img'];

      return $scenarios;
  }

    /**
     * {@inheritdoc}
     */
    public function rules() 
    {
        return [ 
         
            [['created_by', 'modified_by'], 'integer'],
            [['username'], 'unique'], 
            [['email', 'username', 'password'], 'string', 'max' => 45],
            [['full_name'], 'string', 'max' => 125],
            [['auth_key', 'password_reset_token', 'country', 'state', 'city', 'genere', 'timezone'], 'string', 'max' => 64],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usr_id' => Yii::t('app', 'Usr ID'),
            'email' => Yii::t('app', 'Email'),
            'username' => Yii::t('app', 'Correo'),
            'password' => Yii::t('app', 'Password'),
            'full_name' => Yii::t('app', 'Full Name'),
            'auth_key' => Yii::t('app', 'Auth Token'),
            'password_reset_token' => Yii::t('app', 'Recover Token'),
            'country' => Yii::t('app', 'País'),
            'state' => Yii::t('app', 'Estado'),
            'city' => Yii::t('app', 'Ciudad'),
            'genere' => Yii::t('app', 'Genero'),
            'created_by' => Yii::t('app', 'Created By'),
            'modified_by' => Yii::t('app', 'Modified By'),
            'created_at' => Yii::t('app', 'Created At'),
            'last_login' => Yii::t('app', 'Last Login'),
            'modified_at' => Yii::t('app', 'Modified At'),
            'timezone' => Yii::t('app', 'Timezone'),
            'profile_photo' => Yii::t('app', 'Profile Photo'),
            'cover_photo' => Yii::t('app', 'Cover Photo'),
        ];
    }  

    public function validatePassword($password){
         $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
         return $this->security->validatePassword($password,$hash); 
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = $this->security->generateRandomKey() . '_' . time();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function setPassword($password){
        
        $this->password = $this->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = $this->security->generateRandomString(32) . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public function beforeSave($insert)
    { 
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString(32);
            }

            if(isset($this->password)){ 
                $this->password = $this->security->generatePasswordHash($this->password);
            }


            return true;
        }
        return false;
    }

    public function getImagenPerfil($id){
        
        $user = static::findOne($id);
        return  $user ? $user->profile_photo  :  '' ;
    }

    public function getImagenPortada($id){

            $user = static::findOne($id);
            return  $user ? $user->cover_photo  :  '' ;
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */

    public static function find()
    {
        return new UserQuery(get_called_class());
    }
 


    public function obtener($usr_id){
        return (new \yii\db\Query())
           ->select(['usr_id', 'full_name', 'profile_photo', 'cover_photo'])
           ->from('Users')
           ->where('Users.usr_id=:usr_id', [':usr_id' => $usr_id])
           ->limit(1)
           ->all();
    }
    

    public function updateCountry($country,$city){
        $this->country = $country;
        $this->city = $city;
    }

    public function updateProfilePhoto($image){
        $this->profile_photo = $image;
    }

    public function updateCoverPhoto($usr_id,$image){
        $this->cover_photo = $image;
    }
}
