<?php 


namespace app\models;

use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $images;
    public $tempDir =  "storage/tmp/";
    public $storageDir = "/storage/";     

    public function rules()
    {
        return [
            [['images'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif'],
        ];
    }

    
    public function uploadTemp()
    {

        if ($this->validate()) { 
                                   
                $userID = \Yii::$app->user->identity->id;
                $newfile = \Yii::$app->basePath. DIRECTORY_SEPARATOR. $this->tempDir . 'Temp'. $userID. "_" . $this->images->baseName . '.' . $this->images->extension;   
                $newfileThumb = \Yii::$app->basePath. DIRECTORY_SEPARATOR. $this->tempDir . 'Temp_thumb'. $userID. "_" . $this->images->baseName . '.' . $this->images->extension; 
                $this->images->saveAs($newfile);
                Image::thumbnail($newfile, 100, 100)->save($newfileThumb, ['quality' => 50]);
                return $this->images->baseName . '.' . $this->images->extension; /*fileName*/
        } else {
            return false;
        }
    }
    public function savePostImage($file){ 

        $moveFrom =  $newfile =  $this->tempDir . 'Temp'. $userID. "_" . $file;
        $moveMoveto =  $newfile = $this->tempDir . 'Temp'. $userID. "_" . $file;
        rename( $moveFrom , $moveMoveto);

        return true;

    } 


   
}
