<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Post".
 *
 * @property int $post_id
 * @property string $post_content
 * @property int $created_by
 * @property bool $active
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_content'], 'string'],
            [['created_by', 'updated_by'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['active'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'post_content' => 'Post Content',
            'created_by' => 'Created By',
            'active' => 'Active',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function getUser(){

        return $this->hasOne(User::className(), ['usr_id' => 'created_by']);
    }

    public function getProfilePosts($usr_id){

        return 
        $this->find()
        ->select(['full_name' => 'full_name' , 'post_content' => 'post_content', 'post_id' => 'post_id' , 'created_at' => 'Post.created_at', 'created_by' => 'Post.created_by' ])
        ->Where(['Post.wall_target' => $usr_id,'Post.active'=>1,'Post.created_by_group'=>0])
        ->orderBy(['Post.created_at'=>SORT_DESC])
        ->innerJoinWith('user', false)->asArray()->all();
        
    }

    public function getGroupPosts($group_id){

        return 
        $this->find()
        ->select(['full_name' => 'full_name' , 'post_content' => 'post_content', 'post_id' => 'post_id' , 'created_at' => 'Post.created_at', 'created_by' => 'Post.created_by' ])
        ->Where(['Post.wall_target' => $group_id,'Post.active'=>1])
        ->orderBy(['Post.created_at'=>SORT_DESC])
        ->innerJoinWith('user', false)->asArray()->all(); 
        
    }

    public function getHomePosts($usr_id){
        return (new \yii\db\Query())
                   ->select(['Users.full_name', 'Post.post_content', 'Post.post_id', 'Post.created_at', 'Post.created_by'])
                   ->from('Users, Post, myFriends')
                   ->where('Post.active=1')
                   ->andWhere('Post.created_by=myFriends.idFriend')
                   ->andWhere('Users.usr_id=Post.created_by')
                   ->andWhere('myFriends.idUsuario=:usr_id')->addParams([':usr_id' => $usr_id])
                   ->orderBy('Post.created_at DESC')
                   ->limit(50)
                   ->all();
    }

    public function updatePost($post_id,$post_content,$updated_by,$updated_at){
        $request = $this->findOne($post_id);
        $request->post_content = $post_content;
        $request->updated_by = $updated_by;
        $request->updated_at = $updated_at;
        $request->save();
    }

    public function getPostData(){ 
        return 
        $this->find() 
        ->where(['post_id' =>  $this->post_id ])
        ->select(['post_content' => 'post_content' , 'wall_target' => 'wall_target', 'created_by'  => 'Post.created_by' , 'created_by_group' => 'created_by_group','post_id' => 'post_id' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    }

}
