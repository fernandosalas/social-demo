<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "City".
 *
 * @property int $ID_CITY
 * @property string $CC_FIPS
 * @property string $CC_ISO
 * @property string $FULL_NAME_ND
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'City';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CC_FIPS', 'CC_ISO', 'FULL_NAME_ND'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_CITY' => 'Id  City',
            'CC_FIPS' => 'Cc  Fips',
            'CC_ISO' => 'Cc  Iso',
            'FULL_NAME_ND' => 'Full  Name  Nd',
        ];
    }
}
