<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imagen".
 *
 * @property int $imagen_id
 * @property string $nombre_archivo
 * @property int $index
 * @property string $target
 * @property bool $isActive
 * @property string $subida_el
 * @property int $subida_por
 */
class Imagen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    var $base_urlTarget;

    public static function tableName()
    {
        return 'imagen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['index'], 'required'],
            [['imagen_id', 'index', 'subida_por','id_target'], 'integer'],
            [['isActive'], 'boolean'],
            [['subida_el'], 'safe'],
            [['nombre_archivo'], 'string', 'max' => 100],
            [['target'], 'string', 'max' => 10],
            [['imagen_id'], 'unique'],
        ];
    }


    public function  __construct (){
        $user_id  =\Yii::$app->user->identity->id;
        $this->base_urlTarget['post'] =  'http://'.  Yii::$app->getRequest()->serverName . '/web/post/cargar-imagen-completa?usr_id=' . $user_id  ;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'imagen_id' => 'Imagen ID',
            'nombre_archivo' => 'Nombre Archivo',
            'index' => 'Index',
            'target' => 'Target',
            'isActive' => 'Is Active',
            'subida_el' => 'Subida El',
            'subida_por' => 'Subida Por',
        ];
    }

    public function insertar_registro($nombre_archivo, $key , $target, $id_target){
            $this->nombre_archivo = $nombre_archivo;
            $this->index = $key;
            $this->subida_el = date('Y-m-d H:i:s');
            $this->subida_por = \Yii::$app->user->identity->id;
            $this->target = 'post';
            $this->id_target = $id_target;
            $this->save();
    }

     public  function  galleria_post($post_id,$token){
        $imagenes = $this->find()->Where(['id_target' =>  $post_id , 'target' => 'post' ])->asArray()->all();
        foreach($imagenes as $key => $imagen) {
            $baseURL =  'http://'.  Yii::$app->getRequest()->serverName . '/web/post/cargar-imagen-completa?usr_id=' . $imagen['subida_por'] . '&auth_key=' . $token . '&post_id=' . $post_id . '&archivo=';
            $itemGaleria['small'] =   $baseURL . $imagen['nombre_archivo'];
            $itemGaleria['medium'] = $baseURL . $imagen['nombre_archivo'];
            $itemGaleria['big'] =     $baseURL . $imagen['nombre_archivo'];
            $galeria[] = $itemGaleria;
        }

        return $galeria;

     }

}
