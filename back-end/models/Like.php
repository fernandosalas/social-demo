<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Like".
 *
 * @property int $id_like
 * @property int $isActive
 * @property string $type
 * @property int $id_target
 * @property int $like_by
 * @property string $created_at
 * @property string $updated_at
 */
class Like extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Like';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isActive', 'id_target', 'like_by','value'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_like' => 'Id Like',
            'isActive' => 'Is Active',
            'type' => 'Type',
            'id_target' => 'Id Target',
            'like_by' => 'Like By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'value'=> 'value'
        ];
    }
}
