<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Notification".
 *
 * @property int $id_notification
 * @property string $Text
 * @property string $Type
 * @property string $Link
 * @property int $user_target
 * @property string $createt_at
 * @property string $created_at
 * @property string $update_at
 * @property int $created_by
 * @property bool $isActive
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_notification', 'user_target', 'created_by'], 'integer'],
            [['createt_at', 'update_at'], 'safe'],
            [['isActive'], 'boolean'],
            [['id_notification'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_notification' => 'Id Notification',
            'Text' => 'Text',
            'Type' => 'Type',
            'Link' => 'Link',
            'user_target' => 'User Target',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
            'created_by' => 'Created By',
            'isRead' => 'Is Read',
            'readOn' => 'Read On',
            'isActive' => 'Is Active'
        ];
    }

    public function insertNotification($text,$type,$link,$user_target,$created_by){
        $this->Text = $text;
        $this->Type = $type;
        $this->Link = $link;
        $this->user_target = $user_target;
        $this->created_by = $created_by;
        $this->created_at = date('Y-m-d H:i:s');
        $this->isActive = 1;
        $this->isRead = 0;
        return $this->save();
    }

    public function getNotificationData(){
        return 
        $this->find() 
        ->where(['id_notification' =>  $this->id_comment ])
        ->select(['Text' => 'Text' , 'Type' => 'Type', 'Link'  => 'Link' , 'id_notification' => 'id_notification' ] )
        ->asArray()->all();
    }

    public function getNotifications($usr_id){
        return $this->find()
        ->select(['Text' => 'Text' , 'Type' => 'Type', 'Link' => 'Link' , 'id_notification' => 'id_notification' ])
        ->Where(['Notification.user_target' => $usr_id,'Notification.isActive'=>1,'Notification.isRead'=>0])
        ->orderBy(['Notification.created_at'=>SORT_DESC])
        ->limit(10)
        ->asArray()->all();
    }

    public function markAsRead($idNotificacion){
        $request = $this->findOne($idNotificacion);
        $request->isRead = 1;
        $request->readOn = date('Y-m-d H:i:s');
        $request->save();
    }
}
