<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "listGroups".
*
* @property int $idGroup
* @property string $nameGroup
* @property int $createdBy
* @property string $createdAt
*/
class Group extends \yii\db\ActiveRecord
{
    const SCENARIO_NUEVO = 'nuevogrupo';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listGroups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idGroup'], 'required'],
            [['idGroup', 'createdBy'], 'integer'],
            [['createdAt'], 'safe'],
            [['nameGroup'], 'string', 'max' => 25],
            [['idGroup'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idGroup' => 'Id Group',
            'nameGroup' => 'Name Group',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'profile_photo' => 'Profile Photo',
            'cover_photo' => 'Cover Photo'
        ];
    }
    public function scenarios()
    {
    $scenarios = parent::scenarios();
    $scenarios[self::SCENARIO_NUEVO] = ['nameGroup','createdBy'];
    return $scenarios;
    }
    public function getIdGroup(){

        return $this->hasOne(Group::className(), ['idGroup' => 'idGroup'])->from(['idGrupo' => myGroups::tableName()]); 
    } 
    public function getUserInGroup(){
        return $this->hasOne(User::className(), ['usr_id' => 'idUser'])->from(['userGroup' => User::tableName()]);
    }

    public function getMisGrupos($idUsuario){
        $subquery =  $this->find()       
        ->select(['idGroup' =>  'idGroup'])
        ->Where([
            'idUser' => $idUsuario, 
            'aceptada'=>1])
        ->from('myGroups');
        return 
        Group::find()    
        ->select([
            'nameGroup' => 'nameGroup', 
            'idGroup' =>  'idGroup', 
            'profile_photo' =>  'profile_photo', 
            'cover_photo' =>  'cover_photo' 
        ])
        ->Where(['IN', 'idGroup',   $subquery ] )
        ->asArray()->all();
    }

    public function getGruposSugeridos($usr_id){
        $subquery =  MyGroups::find()       
        ->select(['idGroup' =>  'idGroup'])
        ->Where(['idUser' => $usr_id])
        ->from('myGroups');
        return 
        $this->find()    
        ->select(['nameGroup' => 'nameGroup' , 'idGroup' =>  'idGroup', 'profile_photo' =>  'profile_photo'])
        ->Where(['NOT IN', 'idGroup',   $subquery ] )
        ->asArray()->all();
    }


    public function getGruposAdministrados($id_usuario){
        $subquery =  $this->find()      
        ->select(['idGroup' =>  'idGroup'])
        ->Where(['idUser' => $id_usuario,'isAdmin'=>1])
        ->from('myGroups');
        return 
        Group::find()    
        ->select(['nameGroup' => 'nameGroup' , 'idGroup' =>  'idGroup', 'profile_photo' =>  'profile_photo', 'cover_photo' =>  'cover_photo' ])
        ->Where(['IN', 'idGroup',   $subquery ] )
        ->asArray()->all();
    }

    public function getGrupo($id_group){
     return (new \yii\db\Query())
               ->select(['nameGroup', 'idGroup', 'profile_photo', 'cover_photo'])
               ->from('listGroups')
               ->where('idGroup=:id_group', [':id_group' => $id_group])
               ->limit(1)
               ->all();
    }

    public function getSolicitudesGrupos($id_group){
        return (new \yii\db\Query())
                           ->select(['listGroups.nameGroup', 'listGroups.idGroup', 'myGroups.idUser', 'myGroups.aceptada', 'myGroups.rechazada', 'myGroups.idSolicitud', 'Users.full_name', 'Users.profile_photo'])
                           ->from('listGroups, myGroups, Users')
                           ->where('Users.usr_id = myGroups.idUser')
                           ->andWhere('listGroups.idGroup = myGroups.idGroup')
                           ->andWhere('myGroups.isActive=1')
                           ->andWhere('myGroups.aceptada=0')
                           ->andWhere('myGroups.rechazada=0')
                           ->andWhere('listGroups.idGroup=:id_group')->addParams([':id_group' => $id_group])
                           ->all();
    }

    public function getMiembrosGrupos($id_group){
        return (new \yii\db\Query())
                                   ->select(['listGroups.nameGroup', 'listGroups.idGroup', 'myGroups.idUser', 'myGroups.aceptada', 'myGroups.rechazada', 'myGroups.idSolicitud', 'Users.full_name', 'Users.profile_photo'])
                                   ->from('listGroups, myGroups, Users')
                                   ->where('Users.usr_id = myGroups.idUser')
                                   ->andWhere('listGroups.idGroup = myGroups.idGroup')
                                   ->andWhere('myGroups.isActive=1')
                                   ->andWhere('myGroups.aceptada=1')
                                   ->andWhere('myGroups.rechazada=0')
                                   ->andWhere('listGroups.idGroup=:id_group')->addParams([':id_group' => $id_group])
                                   ->all();
    }

    public function updateGroup($idGroup,$nameGroup){
            $request = $this->findOne($idGroup);
            $request->nameGroup = $nameGroup;
            $request->save();
     }

     public function updateProfilePhoto($idGroup,$image){
             $request = $this->findOne($idGroup);
             $request->profile_photo = $image;
             $request->save();
         }

         public function updateCoverPhoto($idGroup,$image){
             $request = $this->findOne($idGroup);
             $request->cover_photo = $image;
             $request->save();
         }

         public function getImagenPerfil($id){

                 $request = static::findOne($id);
                 return  $request ? $request->profile_photo  :  '' ;
             }

             public function getImagenPortada($id){

                     $request = static::findOne($id);
                     return  $request ? $request->cover_photo  :  '' ;
                 }

}
