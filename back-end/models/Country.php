<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Country".
 *
 * @property int $ID_country
 * @property string $cc_fips
 * @property string $cc_iso
 * @property string $tld
 * @property string $country_name
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cc_fips', 'cc_iso'], 'string', 'max' => 2],
            [['tld'], 'string', 'max' => 3],
            [['country_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_country' => 'Id Country',
            'cc_fips' => 'Cc Fips',
            'cc_iso' => 'Cc Iso',
            'tld' => 'Tld',
            'country_name' => 'Country Name',
        ];
    }
}
