<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "myFriends".
 *
 * @property int $idUsuario
 * @property int $idFriend
 * @property bool $isActive
 * @property string $amigos_desde
 * @property int $actualizado_por
 * @property string $actualizado_el
 * @property bool $aceptada
 * @property bool $rechazada
 */
class MyFriends extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'myFriends';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idFriend', 'actualizado_por'], 'required'],
            [['idUsuario', 'idFriend', 'actualizado_por'], 'integer'],
            [['isActive', 'aceptada', 'rechazada'], 'boolean'],
            [['amigos_desde', 'actualizado_el'], 'safe'],
            [['idUsuario', 'idFriend'], 'unique', 'targetAttribute' => ['idUsuario', 'idFriend']],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idFriend' => 'Id Friend',
            'isActive' => 'Is Active',
            'amigos_desde' => 'Amigos Desde',
            'actualizado_por' => 'Actualizado Por',
            'actualizado_el' => 'Actualizado El',
            'aceptada' => 'Aceptada',
            'rechazada' => 'Rechazada',
        ];
    }

    public function getProfileFriends($usr_id){

        return 
        $this->find()
        ->select(['full_name' => 'full_name'  ])
        ->Where(['Post.created_by' => $usr_id,'Post.active'=>1])
        ->orderBy(['Post.created_at'=>SORT_DESC])
        ->innerJoinWith('user', false)->asArray()->all();
        
    }

}
