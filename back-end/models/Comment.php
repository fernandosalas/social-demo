<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Comment".
 *
 * @property int $id_comment
 * @property string $content
 * @property bool $isActive
 * @property string $create_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $type
 * @property int $id_target
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['isActive'], 'boolean'],
            [['create_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'id_target'], 'integer'],
            [['type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_comment' => 'Id comment',
            'content' => 'Content',
            'isActive' => 'Is Active',
            'create_at' => 'Create At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'type' => 'Type',
            'id_target' => 'Id Target',
        ];
    }

    public function getUser(){
        
        return $this->hasOne(User::className(), ['usr_id' => 'created_by']);
    }


    public function getPostsComments($usr_id, $id_target){
        return 
        $this->find() 
        ->where(['usr_id' => $usr_id, 'id_target' => $id_target, 'isActive'=>1 ])
        ->select(['full_name' => 'full_name' , 'content' => 'content', 'create_at'  => 'create_at' , 'id_comment' => 'id_comment' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    } 

    public function getGroupPostsComments($id_target){
        return 
        $this->find() 
        ->where(['id_target' => $id_target, 'isActive'=>1 ])
        ->select(['full_name' => 'full_name' , 'content' => 'content', 'create_at'  => 'create_at' , 'id_comment' => 'id_comment' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    } 


    public function getHomePostsComments($id_target){
        return 
        $this->find() 
        ->where(['id_target' => $id_target, 'isActive' => 1, 'type' => 'comment' ])
        ->select(['full_name' => 'full_name' , 'content' => 'content', 'create_at'  => 'create_at' , 'id_comment' => 'id_comment', 'created_by' => 'Comment.created_by' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    }

    public function getResponseComments($id_target){
        return 
        $this->find() 
        ->where(['id_target' => $id_target, 'isActive' => 1, 'type' => 'response' ])
        ->select(['full_name' => 'full_name' , 'content' => 'content', 'create_at'  => 'create_at' , 'id_comment' => 'id_comment', 'created_by' => 'Comment.created_by' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    }

     public function getCommentData(){
        return 
        $this->find() 
        ->where(['id_comment' =>  $this->id_comment ])
        ->select(['full_name' => 'full_name' , 'content' => 'content', 'create_at'  => 'create_at' , 'id_comment' => 'id_comment' ] )
        ->innerJoinWith('user', false)->asArray()->all();
    }

    public function insertResponse($id_target,$content,$type,$created_by,$isActive){
        $this->id_target = $id_target;
        $this->content = $content;
        $this->type = $type;
        $this->created_by = $created_by;
        $this->create_at = date('Y-m-d H:i:s');
        $this->isActive = $isActive;
        $this->save();
    }
}
