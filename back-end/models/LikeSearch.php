<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Like;

/**
 * LikeSearch represents the model behind the search form of `app\models\Like`.
 */
class LikeSearch extends Like
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_like', 'isActive', 'id_target', 'like_by','value'], 'integer'],
            [['type', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Like::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_like' => $this->id_like,
            'isActive' => $this->isActive,
            'id_target' => $this->id_target,
            'like_by' => $this->like_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
