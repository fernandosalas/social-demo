<?php

namespace app\models;
use app\models\User;

use Yii;

/**
 * This is the model class for table "myFriends".
 *
 * @property int $idUsuario
 * @property int $idFriend
 * @property bool $isActive
 * @property string $amigos_desde
 * @property string $actualizado_por
 * @property string $actualizado_el
 */
class Friends extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'myFriends';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idFriend', 'actualizado_por'], 'integer'],
            [['idFriend'], 'required'],
            [['isActive', 'aceptada', 'rechazada'], 'boolean'],
            [['amigos_desde', 'actualizado_el'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'idFriend' => 'Id Friend',
            'isActive' => 'Is Active',
            'amigos_desde' => 'Amigos Desde',
            'actualizado_por' => 'Actualizado Por',
            'actualizado_el' => 'Actualizado El',
        ]; 
    }
    public function getUser(){

        return $this->hasOne(User::className(), ['usr_id' => 'idUsuario'])->from(['user' => User::tableName()]); ;
    }  

    public function getFriend(){
        return $this->hasOne(User::className(), ['usr_id' => 'idFriend'])->from(['friend' => User::tableName()]);
    }    
    // Esta funcion es para cuando la otra persona me agrego
    public function getFriend2(){
        return $this->hasOne(User::className(), ['usr_id' => 'idUsuario'])->from(['friend2' => User::tableName()]);
    }

    public function getProfileFriends($usr_id){

        
        $query1 =  $this->find()                                       //alias        'campo tabla'     
        ->select(['friendUsername' => 'friend.username' , 'idFriend' =>  'myFriends.idFriend' , 'full_name' => 'friend.full_name' ])
        ->Where(['myFriends.idUsuario' => $usr_id, 'isActive'=>1, 'aceptada'=>1])
        ->orderBy(['amigos_desde'=>SORT_DESC])
        ->innerJoinWith('friend', false);

        $query2 =  $this->find()                                       //alias        'campo tabla'     
        ->select(['friendUsername' => 'friend2.username' , 'idFriend' =>  'myFriends.idUsuario' , 'full_name' => 'friend2.full_name' ])
        ->Where(['myFriends.idFriend' => $usr_id, 'isActive'=>1, 'aceptada'=>1])
        ->orderBy(['amigos_desde'=>SORT_DESC])
        ->innerJoinWith('friend2', false);


        return  $unionQuery = (new \yii\db\Query())
        ->from(['listaAmigos' => $query1->union($query2)])
        ->orderBy(['friendUsername' => SORT_ASC])
        ->all();
        
    }

    public function getProfileAddFriends($id_usuario){
       
        $subquery =  $this->find()  //alias 'campo tabla'     
        ->select(['idFriend' =>  'myFriends.idFriend'])
        ->Where(['myFriends.idUsuario' => $id_usuario, 'isActive'=> 1, 'aceptada' => 1 ])
        //->andWhere(['<>', 'aceptada',  0 ]).
        ->orderBy(['amigos_desde'=>SORT_DESC])
        ->innerJoinWith('user', false)
        ->innerJoinWith('friend', false);
        $notIn[] = $id_usuario;

        //obtener solicitudes de amistad activas
        $subquery2 = (new \yii\db\Query())
                   ->select(['idFriend' =>'myFriends.idUsuario'])
                   ->from('myFriends, Users')
                   ->where('myFriends.isActive=1')
                   ->andWhere('Users.usr_id = myFriends.idUsuario')
                   ->andWhere('myFriends.aceptada=0')
                   ->andWhere('myFriends.rechazada=0')
                   ->andWhere('myFriends.idFriend=:usr_id')->addParams([':usr_id' => $id_usuario])
                   ->all();

                   ///print_r($subquery2);exit();

        return 
        User::find()  
        //alias        'campo tabla'     
        ->select([
         'friendUsername' => 'username' ,
         'usr_id' =>  'usr_id',
         'idFriend' =>  'idFriend',
         'full_name' =>  'full_name',
         'aceptada' =>  'aceptada',
         'rechazada' =>  'rechazada' 
        ])
        ->join('LEFT JOIN', 'myFriends','myFriends.idFriend = Users.usr_id AND  myFriends.idUsuario = ' . $id_usuario)
        ->Where(['NOT IN', 'usr_id',   $notIn ] )
        ->andWhere(['NOT IN', 'usr_id',  $subquery ]) // remover a quienes no son mi amigos 
        ->andWhere(['NOT IN', 'idFriend',  $subquery2 ]) //Remover solicitudes de amistad activas
        ->asArray()->all();
        
    }    

    public function getSolicitudesAmistad($usr_id){
  
        return (new \yii\db\Query())
                   ->select(['myFriends.idSolicitud', 'myFriends.idUsuario', 'myFriends.idFriend', 'Users.full_name AS nombre'])
                   ->from('myFriends, Users')
                   ->where('myFriends.isActive=1')
                   ->andWhere('Users.usr_id = myFriends.idUsuario')
                   ->andWhere('myFriends.aceptada=0')
                   ->andWhere('myFriends.rechazada=0')
                   ->andWhere('myFriends.idFriend=:usr_id')->addParams([':usr_id' => $usr_id])
                   ->all();
    }

    public function aceptarSolicitud($solicitud_id,$idUsuario){
        $request = $this->findOne($solicitud_id);
        $request->aceptada = 1;
        $request->actualizado_el = date('Y-m-d H:i:s');
        $request->actualizado_por = $idUsuario;
        return $request->save();
    }

    public function rechazarSolicitud($solicitud_id,$idUsuario){
        $request = $this->findOne($solicitud_id);
        $request->rechazada = 1;
        $request->actualizado_el = date('Y-m-d H:i:s');
        $request->actualizado_por = $idUsuario;
        $request->save();
    }

    public function guardarSolicitud($idUsuario,$idFriend){
        $this->idUsuario = $idUsuario;
        $this->idFriend = $idFriend;
        $this->actualizado_por = $idUsuario;
        $this->aceptada = 0;
        $this->rechazada = 0;
        $this->isActive = 1;
        $this->actualizado_el = date('Y-m-d H:i:s');
        return $this->save();
    }
}
